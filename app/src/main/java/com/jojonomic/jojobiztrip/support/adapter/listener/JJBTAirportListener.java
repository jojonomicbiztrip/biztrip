package com.jojonomic.jojobiztrip.support.adapter.listener;

/**
 * @author Muhammad Umar Farisi
 * @created 18/04/2017
 */
public interface JJBTAirportListener {
    void airportSelected(int position);
}
