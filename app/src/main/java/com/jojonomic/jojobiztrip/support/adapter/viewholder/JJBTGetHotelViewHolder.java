package com.jojonomic.jojobiztrip.support.adapter.viewholder;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTHotelModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTGetHotelListener;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJURectangleImageView;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

/**
 * Created by Asus on 04/03/2017.
 */
public class JJBTGetHotelViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private JJUTextView nameHotel;
    private JJUTextView placeHotel;
    private JJUTextView oldPriceHotel;
    private JJUTextView newPriceHotel;
    private JJURectangleImageView[] ratingsImageView;

    private JJBTGetHotelListener listener;

    private int position;


    public JJBTGetHotelViewHolder(View itemView, JJBTGetHotelListener listener) {
        super(itemView);
        this.listener = listener;
        this.itemView.setOnClickListener(this);
        this.position = -1;
        this.ratingsImageView = new JJURectangleImageView[5];
        loadView();
        setUpPropertiesOfView();
    }

    private void loadView() {
        nameHotel = (JJUTextView) itemView.findViewById(R.id.get_hotel_name);
        placeHotel = (JJUTextView) itemView.findViewById(R.id.get_hotel_place);
        oldPriceHotel = (JJUTextView) itemView.findViewById(R.id.get_hotel_old_price);
        newPriceHotel = (JJUTextView) itemView.findViewById(R.id.get_hotel_new_price);
        ratingsImageView[0] = (JJURectangleImageView) itemView.findViewById(R.id.get_hotel_rating_one);
        ratingsImageView[1] = (JJURectangleImageView) itemView.findViewById(R.id.get_hotel_rating_two);
        ratingsImageView[2] = (JJURectangleImageView) itemView.findViewById(R.id.get_hotel_rating_three);
        ratingsImageView[3] = (JJURectangleImageView) itemView.findViewById(R.id.get_hotel_rating_four);
        ratingsImageView[4] = (JJURectangleImageView) itemView.findViewById(R.id.get_hotel_rating_five);
    }

    private void setUpPropertiesOfView() {
        oldPriceHotel.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void setUpData(JJBTHotelModel data, int position){
        this.position = position;
        nameHotel.setText(data.getHotelName());
        placeHotel.setText(data.getHotelAddress());
        oldPriceHotel.setText(data.getHotelOldPrice());
        newPriceHotel.setText(data.getHotelNewPrice());
        for(int i = 0; i < data.getHotelRating() ; i++){
            ratingsImageView[i].setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(View v) {
        if(position != -1){
            listener.onHotelChoose(position);
        }
    }
}
