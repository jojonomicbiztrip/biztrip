package com.jojonomic.jojobiztrip.support.adapter.listener;

/**
 * Created by Asus on 08/03/2017.
 */
public interface JJBTPassengerFillInDetailsListener {
    void onChangePassenger(int index);
}
