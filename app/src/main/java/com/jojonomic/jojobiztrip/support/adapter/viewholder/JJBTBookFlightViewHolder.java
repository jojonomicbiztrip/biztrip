package com.jojonomic.jojobiztrip.support.adapter.viewholder;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTBookFlightListener;
import com.jojonomic.jojobiztrip.utilities.helper.JJBTDurationHelper;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;
import com.jojonomic.jojoutilitieslib.utilities.helper.JJUCurrencyHelper;

/**
 * Created by Asus on 02/03/2017.
 */
public class JJBTBookFlightViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private JJUTextView airlinesNameFlight;
    private JJUTextView scheduleFlight;
    private JJUTextView durationFlight;
    private JJUTextView totalPriceFlight;
    private JJUTextView realPriceFlight;

    private JJBTBookFlightListener listener;

    private int position;


    public JJBTBookFlightViewHolder(View itemView, JJBTBookFlightListener listener) {
        super(itemView);
        this.listener = listener;
        this.itemView.setOnClickListener(this);
        this.position = -1;
        loadView();
        setUpPropertiesOfView();

    }

    private void loadView() {
        airlinesNameFlight = (JJUTextView) itemView.findViewById(R.id.book_flight_airlines_name);
        scheduleFlight = (JJUTextView) itemView.findViewById(R.id.book_flight_schedule);
        durationFlight = (JJUTextView) itemView.findViewById(R.id.book_flight_duration);
        totalPriceFlight = (JJUTextView) itemView.findViewById(R.id.book_flight_total_price);
        realPriceFlight = (JJUTextView) itemView.findViewById(R.id.book_flight_real_price);
    }

    private void setUpPropertiesOfView() {
        totalPriceFlight.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void setUpData(JJBTFlightModel data, int position, Context context){
        this.position = position;
        airlinesNameFlight.setText(data.getFlightAirlinesName());
        scheduleFlight.setText(data.getFlightDepartureTime()+ " - "+data.getFlightArrivalTime());
        durationFlight.setText(JJBTDurationHelper.getFlightDurationFormatted(data.getFlightDuration()));
        totalPriceFlight.setText( JJUCurrencyHelper
                .generateCurrencyFormatter(context,"IDR")
                .format(data.getFlightTotalPrice()));
        realPriceFlight.setText(JJUCurrencyHelper
                .generateCurrencyFormatter(context,"IDR")
                .format(data.getFlightRealPrice()));
    }


    @Override
    public void onClick(View v) {
        if(position != -1){
            listener.onFlightChoose(position);
        }
    }
}
