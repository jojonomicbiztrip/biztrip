package com.jojonomic.jojobiztrip.support.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.support.dialog.listener.JJBTSortAlertDialogListener;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUButton;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

/**
 * Created by Asus on 05/03/2017.
 */
public class JJBTSortAlertDialog extends AlertDialog implements View.OnClickListener {

    public static final int SORTED_BY_PRICE_ID = R.id.sort_price_radio_button;
    public static final int SORTED_BY_TIME_ID = R.id.sort_time_radio_button;
    public static final int SORTED_BY_NAME_ID = R.id.sort_name_radio_button;

    private JJUTextView titleTextView;
    private RadioGroup radioGroup;
    private RadioButton nameRadioButton;
    private RadioButton priceRadioButton;
    private RadioButton timeRadioButton;
    private JJUButton sortButton;

    private JJBTSortAlertDialogListener listener;


    public JJBTSortAlertDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_sort);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        titleTextView = (JJUTextView) findViewById(R.id.sort_title_text_view);
        radioGroup = (RadioGroup) findViewById(R.id.sort_radio_group);
        nameRadioButton = (RadioButton) findViewById(R.id.sort_name_radio_button);
        priceRadioButton = (RadioButton) findViewById(R.id.sort_price_radio_button);
        timeRadioButton = (RadioButton) findViewById(R.id.sort_time_radio_button);
        sortButton = (JJUButton) findViewById(R.id.sort_button);
    }

    private void initiateDefaultValue() {
        sortButton.setOnClickListener(this);
    }

    public void setListener(JJBTSortAlertDialogListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sort_button:
                dismiss();
                listener.onSortedBy(radioGroup.getCheckedRadioButtonId());
                break;
        }
    }
}
