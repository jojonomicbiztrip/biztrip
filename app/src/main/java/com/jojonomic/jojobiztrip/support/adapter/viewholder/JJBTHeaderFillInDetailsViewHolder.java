package com.jojonomic.jojobiztrip.support.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.model.JJBTContactModel;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTHeaderFillInDetailsListener;
import com.jojonomic.jojobiztrip.utilities.helper.JJBTDurationHelper;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUEditText;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;
import com.jojonomic.jojoutilitieslib.utilities.helper.JJUFormatData;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author Muhammad Umar Farisi
 * @created 12/04/2017
 */
public class JJBTHeaderFillInDetailsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private LinearLayout returnContainerLinearLayout;
    private JJUEditText contactNameEditText;
    private JJUEditText contactNumberEditText;
    private JJUEditText contactEmailEditText;
    private ImageButton addPassengerButton;

    private JJUTextView departFlightRouteTextView;
    private JJUTextView departFlightDateTextView;
    private JJUTextView departFlightTimeTextView;
    private JJUTextView departFlightDurationTextView;

    private JJUTextView returnFlightRouteTextView;
    private JJUTextView returnFlightDateTextView;
    private JJUTextView returnFlightTimeTextView;
    private JJUTextView returnFlightDurationTextView;

    private JJBTHeaderFillInDetailsListener listener;

    private JJBTContactModel model;

    public JJBTHeaderFillInDetailsViewHolder(View itemView
            , JJBTHeaderFillInDetailsListener listener
            , JJBTContactModel model) {
        super(itemView);
        this.model = model;
        this.listener = listener;
        loadView();
        setUpPropertiesOfView();
    }

    private void setUpPropertiesOfView() {
        addPassengerButton.setOnClickListener(this);
        contactNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                model.setContactName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        contactNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                model.setContactNumber(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        contactEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                model.setContactEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void loadView() {
        returnContainerLinearLayout = (LinearLayout) itemView.findViewById(R.id.fill_in_details_return_container_linear_layout);
        contactNameEditText = (JJUEditText) itemView.findViewById(R.id.fill_in_details_contact_name_edit_text);
        contactNumberEditText = (JJUEditText) itemView.findViewById(R.id.fill_in_details_contact_number_edit_text);
        contactEmailEditText = (JJUEditText) itemView.findViewById(R.id.fill_in_details_contact_email_edit_text);
        addPassengerButton = (ImageButton) itemView.findViewById(R.id.fill_in_details_add_passenger_button);

        departFlightRouteTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_depart_flight_route_text_view);
        departFlightDateTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_depart_flight_date_text_view);
        departFlightTimeTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_depart_flight_time_text_view);
        departFlightDurationTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_depart_flight_duration_text_view);

        returnFlightRouteTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_return_flight_route_text_view);
        returnFlightDateTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_return_flight_date_text_view);
        returnFlightTimeTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_return_flight_time_text_view);
        returnFlightDurationTextView = (JJUTextView) itemView.findViewById(R.id.fill_in_details_return_flight_duration_text_view);
    }

    public void setUpData(boolean isRoundTrip,
                          JJBTAirportModel originAirport,
                          JJBTAirportModel destinationAirport,
                          JJBTFlightModel departFlight,
                          JJBTFlightModel returnFlight,
                          Calendar departureDate,
                          Calendar returnDate){

        DateFormatSymbols dateSymbols = new DateFormatSymbols(Locale.getDefault());
        if(isRoundTrip){
            returnContainerLinearLayout.setVisibility(View.VISIBLE);
            returnFlightRouteTextView.setText(destinationAirport.getAirportCode()+" - "+originAirport.getAirportCode());
            returnFlightDateTextView.setText(dateSymbols.getWeekdays()[returnDate.get(Calendar.DAY_OF_WEEK)]+", "+ JJUFormatData
                    .convertDateTimeToString(JJUFormatData.DATE_FORMAT_UI_V2,returnDate.getTime().getTime()));
            returnFlightTimeTextView.setText(returnFlight.getFlightDepartureTime()+" - "+returnFlight.getFlightArrivalTime());
            returnFlightDurationTextView.setText(JJBTDurationHelper.getFlightDurationFormatted(returnFlight.getFlightDuration()));
        }
        departFlightRouteTextView.setText(originAirport.getAirportCode()+" - "+destinationAirport.getAirportCode());
        departFlightDateTextView.setText(dateSymbols.getWeekdays()[departureDate.get(Calendar.DAY_OF_WEEK)]+", "+JJUFormatData
                .convertDateTimeToString(JJUFormatData.DATE_FORMAT_UI_V2,departureDate.getTime().getTime()));
        departFlightTimeTextView.setText(departFlight.getFlightDepartureTime()+" - "+departFlight.getFlightArrivalTime());
        departFlightDurationTextView.setText(JJBTDurationHelper.getFlightDurationFormatted(departFlight.getFlightDuration()));
    }

    @Override
    public void onClick(View v) {
        listener.addPassenger();
    }
}
