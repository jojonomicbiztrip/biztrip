package com.jojonomic.jojobiztrip.support.adapter.listener;

/**
 * Created by Asus on 24/02/2017.
 */
public interface JJBTBookFlightListener {
    void onFlightChoose(int position);
}
