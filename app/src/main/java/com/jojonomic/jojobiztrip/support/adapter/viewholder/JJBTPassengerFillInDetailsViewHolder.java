package com.jojonomic.jojobiztrip.support.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTPassengerFillInDetailsListener;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

/**
 * Created by Asus on 08/03/2017.
 */
public class JJBTPassengerFillInDetailsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private JJUTextView passengerName;
    private JJUTextView changeButton;

    private JJBTPassengerFillInDetailsListener listener;

    private int index;

    public JJBTPassengerFillInDetailsViewHolder(View itemView, JJBTPassengerFillInDetailsListener listener) {
        super(itemView);
        this.listener = listener;
        this.index = -1;
        loadView();
        setUpPropertiesOfView();
    }

    private void loadView() {
        passengerName = (JJUTextView) itemView.findViewById(R.id.passenger_details_passenger_name);
        changeButton = (JJUTextView) itemView.findViewById(R.id.passenger_details_change_button);
    }

    private void setUpPropertiesOfView() {
        changeButton.setOnClickListener(this);
    }

    public void setUpData(String name, int index){
        passengerName.setText(name);
        this.index = index;
    }

    @Override
    public void onClick(View v) {
        if(index != -1){
            listener.onChangePassenger(index);
        }
    }
}
