package com.jojonomic.jojobiztrip.support.adapter.listener;

/**
 * Created by Asus on 01/03/2017.
 */
public interface JJBTGetHotelListener {
    void onHotelChoose(int position);
}
