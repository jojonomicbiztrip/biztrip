package com.jojonomic.jojobiztrip.support.dialog.listener;

/**
 * Created by Asus on 05/03/2017.
 */
public interface JJBTSortAlertDialogListener {
    void onSortedBy(int idRadioButton);
}
