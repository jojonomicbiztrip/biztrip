package com.jojonomic.jojobiztrip.support.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTHotelModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTGetHotelListener;
import com.jojonomic.jojobiztrip.support.adapter.viewholder.JJBTGetHotelViewHolder;

import java.util.List;

/**
 * Created by Asus on 01/03/2017.
 */
public class JJBTGetHotelAdapter extends RecyclerView.Adapter<JJBTGetHotelViewHolder> {

    private List<JJBTHotelModel> hotels;
    private Context context;
    private JJBTGetHotelListener listener;

    public JJBTGetHotelAdapter(List<JJBTHotelModel> hotels, Context context, JJBTGetHotelListener listener) {
        this.hotels = hotels;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public JJBTGetHotelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_get_hotel,parent,false);
        return new JJBTGetHotelViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(JJBTGetHotelViewHolder holder, int position) {
        holder.setUpData(hotels.get(position),position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return hotels.size();
    }

}
