package com.jojonomic.jojobiztrip.support.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTAirportListener;
import com.jojonomic.jojobiztrip.utilities.helper.JJBTAirportHelper;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

/**
 * @author Muhammad Umar Farisi
 * @created 17/04/2017
 */
public class JJBTAirportViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private JJUTextView airportTextView;
    private JJBTAirportListener listener;
    private int position;

    public JJBTAirportViewHolder(View itemView, JJBTAirportListener listener) {
        super(itemView);
        airportTextView = (JJUTextView) itemView.findViewById(R.id.airport_name);
        this.listener = listener;
        this.position = -1;
        this.itemView.setOnClickListener(this);
    }

    public void setUpData(JJBTAirportModel data, int position){
        airportTextView.setText(JJBTAirportHelper.getAirportVisibleFormatted(data));
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        if(position != -1)listener.airportSelected(position);
    }
}
