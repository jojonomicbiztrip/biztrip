package com.jojonomic.jojobiztrip.support.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTBookFlightListener;
import com.jojonomic.jojobiztrip.support.adapter.viewholder.JJBTBookFlightViewHolder;

import java.util.List;

/**
 * Created by Asus on 24/02/2017.
 */
public class JJBTBookFlightAdapter extends RecyclerView.Adapter<JJBTBookFlightViewHolder> {

    private List<JJBTFlightModel> flights;
    private Context context;
    private JJBTBookFlightListener listener;

    public JJBTBookFlightAdapter(List<JJBTFlightModel> flights, Context context, JJBTBookFlightListener listener) {
        this.flights = flights;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public JJBTBookFlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_book_flight,parent,false);
        return new JJBTBookFlightViewHolder(view,listener);
    }

    @Override
    public void onBindViewHolder(JJBTBookFlightViewHolder holder, int position) {
        holder.setUpData(flights.get(position),position, context);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return flights.size();
    }
}
