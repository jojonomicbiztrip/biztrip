package com.jojonomic.jojobiztrip.support.adapter.listener;

/**
 * @author Muhammad Umar Farisi
 * @created 12/04/2017
 */
public interface JJBTHeaderFillInDetailsListener {
    void addPassenger();
}
