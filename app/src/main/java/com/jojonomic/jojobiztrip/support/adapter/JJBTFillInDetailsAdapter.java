package com.jojonomic.jojobiztrip.support.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.model.JJBTContactModel;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTHeaderFillInDetailsListener;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTPassengerFillInDetailsListener;
import com.jojonomic.jojobiztrip.support.adapter.viewholder.JJBTHeaderFillInDetailsViewHolder;
import com.jojonomic.jojobiztrip.support.adapter.viewholder.JJBTPassengerFillInDetailsViewHolder;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Asus on 08/03/2017.
 */
public class JJBTFillInDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int FILL_IN_DETAILS_HEADER_TYPE = -1;
    private static final int FILL_IN_DETAILS_PASSENGER_TYPE = 0;
    private static final int EXTRA_HEADER = 1;

    private List<String> passengers;
    private Context context;
    private JJBTHeaderFillInDetailsListener headerListener;
    private JJBTPassengerFillInDetailsListener passengerListener;

    private JJBTContactModel contactModel;
    private boolean isRoundTrip;
    private JJBTAirportModel originAirport;
    private JJBTAirportModel destinationAirport;
    private JJBTFlightModel departFlight;
    private JJBTFlightModel returnFlight;
    private Calendar departureDate;
    private Calendar returnDate;

    public JJBTFillInDetailsAdapter(Context context
            , JJBTHeaderFillInDetailsListener headerListener
            , JJBTPassengerFillInDetailsListener passengerListener
            , JJBTContactModel contactModel
            , boolean isRoundTrip
            , JJBTAirportModel originAirport
            , JJBTAirportModel destinationAirport
            , JJBTFlightModel departFlight
            , JJBTFlightModel returnFlight
            , Calendar departureDate
            , Calendar returnDate
            , List<String> passengers){
        this.passengers = passengers;
        this.context = context;
        this.headerListener = headerListener;
        this.passengerListener = passengerListener;
        this.contactModel = contactModel;
        this.isRoundTrip = isRoundTrip;
        this.originAirport = originAirport;
        this.destinationAirport = destinationAirport;
        this.departFlight = departFlight;
        this.returnFlight = returnFlight;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position){
            case 0:
                return FILL_IN_DETAILS_HEADER_TYPE;
            default:
                return FILL_IN_DETAILS_PASSENGER_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case FILL_IN_DETAILS_HEADER_TYPE:
                return new JJBTHeaderFillInDetailsViewHolder(LayoutInflater
                        .from(context)
                        .inflate(R.layout.adapter_header_fill_in_details, parent, false), headerListener, contactModel);
            default:
                return new JJBTPassengerFillInDetailsViewHolder(LayoutInflater
                        .from(context)
                        .inflate(R.layout.adapter_passenger_fill_in_details, parent, false), passengerListener);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof JJBTHeaderFillInDetailsViewHolder){
            ((JJBTHeaderFillInDetailsViewHolder) holder).setUpData(isRoundTrip
                    , originAirport
                    , destinationAirport
                    , departFlight
                    , returnFlight
                    , departureDate
                    , returnDate);
        }else{
            ((JJBTPassengerFillInDetailsViewHolder)holder).setUpData(passengers.get(position-EXTRA_HEADER), position-EXTRA_HEADER);
        }
    }

    @Override
    public int getItemCount() {
        return passengers.size()+EXTRA_HEADER;
    }
}
