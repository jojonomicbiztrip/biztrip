package com.jojonomic.jojobiztrip.support.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTAirportListener;
import com.jojonomic.jojobiztrip.support.adapter.viewholder.JJBTAirportViewHolder;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Muhammad Umar Farisi
 * @created 05/04/2017
 */
public class JJBTAirportAdapter extends RecyclerView.Adapter<JJBTAirportViewHolder>{

    private Context context;
    private List<JJBTAirportModel> airports;
    private JJBTAirportListener listener;

    public JJBTAirportAdapter(Context context, List<JJBTAirportModel> airports, JJBTAirportListener listener) {
        this.context = context;
        this.airports = airports;
        this.listener =listener;
    }

    @Override
    public JJBTAirportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new JJBTAirportViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_airport,parent,false),listener);
    }

    @Override
    public void onBindViewHolder(JJBTAirportViewHolder holder, int position) {
        holder.setUpData(airports.get(position),position);
    }

    @Override
    public int getItemCount() {
        return airports.size();
    }
}
