package com.jojonomic.jojobiztrip.screen.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTFilterController;
import com.jojonomic.jojobiztrip.support.extentions.view.JJBTNumberPicker;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUButton;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUEditText;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJURectangleImageView;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;
import com.jojonomic.jojoutilitieslib.support.extentions.watcher.JJUDecimalTextWatcher;
import com.jojonomic.jojoutilitieslib.support.extentions.watcher.listener.JJUTextWatcherListener;

public class JJBTFilterActivity extends AppCompatActivity implements View.OnClickListener {

    private AppBarLayout appBar;
    private JJUButton filterButton;
    private JJURectangleImageView refreshFilteredByPriceButton;
    private JJURectangleImageView refreshFilteredByTimeButton;
    private JJUEditText lowerPriceEditText;
    private JJUEditText higherPriceEditText;
    private JJBTNumberPicker fromHourNumberPicker;
    private JJBTNumberPicker fromMinuteNumberPicker;
    private JJBTNumberPicker toHourNumberPicker;
    private JJBTNumberPicker toMinuteNumberPicker;

    private JJBTFilterController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        filterButton = (JJUButton) findViewById(R.id.filter_button);
        refreshFilteredByPriceButton = (JJURectangleImageView) findViewById(R.id.filter_refresh_filtered_by_price_button);
        refreshFilteredByTimeButton = (JJURectangleImageView) findViewById(R.id.filter_refresh_filtered_by_time_button);
        lowerPriceEditText = (JJUEditText) findViewById(R.id.filter_lower_price_edit_text);
        higherPriceEditText = (JJUEditText) findViewById(R.id.filter_higher_price_edit_text);
        fromHourNumberPicker = (JJBTNumberPicker) findViewById(R.id.filter_from_hour_number_picker);
        fromMinuteNumberPicker = (JJBTNumberPicker) findViewById(R.id.filter_from_minute_number_picker);
        toHourNumberPicker = (JJBTNumberPicker) findViewById(R.id.filter_to_hour_number_picker);
        toMinuteNumberPicker = (JJBTNumberPicker) findViewById(R.id.filter_to_minute_number_picker);
    }

    private void initiateDefaultValue() {


        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.filtered_by);
        appBar.findViewById(R.id.toolbar_back_image_button).setOnClickListener(this);

        filterButton.setOnClickListener(this);
        refreshFilteredByPriceButton.setOnClickListener(this);
        refreshFilteredByTimeButton.setOnClickListener(this);

        fromHourNumberPicker.setValue(0,24,0);
        fromMinuteNumberPicker.setValue(0,60,0);
        toHourNumberPicker.setValue(0,24,0);
        toMinuteNumberPicker.setValue(0,60,0);

        //set controller
        controller = new JJBTFilterController(this);

        configureAmountTextWatcher();
    }

    public void configureAmountTextWatcher() {

        JJUDecimalTextWatcher lowerPriceTextWatcher =
                new JJUDecimalTextWatcher(lowerPriceEditText, "id_ID", new JJUTextWatcherListener() {
            @Override
            public void onTextValueChanged(Number lowerPrice) {
                    if (isControllerNotNull()) {
                        controller.onLowerPriceChange(lowerPrice);
                    }
            }
        });
        lowerPriceEditText.addTextChangedListener(lowerPriceTextWatcher);

        JJUDecimalTextWatcher higherPriceTextWatcher =
                new JJUDecimalTextWatcher(higherPriceEditText, "id_ID", new JJUTextWatcherListener() {
            @Override
            public void onTextValueChanged(Number higherPrice) {
                    if (isControllerNotNull()) {
                        controller.onHigherPriceChange(higherPrice);
                    }
            }
        });
        higherPriceEditText.addTextChangedListener(higherPriceTextWatcher);

    }

    public JJUEditText getLowerPriceEditText() {
        return lowerPriceEditText;
    }

    public JJUEditText getHigherPriceEditText() {
        return higherPriceEditText;
    }

    public JJBTNumberPicker getFromHourNumberPicker() {
        return fromHourNumberPicker;
    }

    public JJBTNumberPicker getFromMinuteNumberPicker() {
        return fromMinuteNumberPicker;
    }

    public JJBTNumberPicker getToHourNumberPicker() {
        return toHourNumberPicker;
    }

    public JJBTNumberPicker getToMinuteNumberPicker() {
        return toMinuteNumberPicker;
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

}
