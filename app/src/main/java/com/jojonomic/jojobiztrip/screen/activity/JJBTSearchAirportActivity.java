package com.jojonomic.jojobiztrip.screen.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTSearchAirportController;
import com.jojonomic.jojobiztrip.support.adapter.JJBTAirportAdapter;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTAirportListener;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUEditText;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

import java.util.List;

public class JJBTSearchAirportActivity extends AppCompatActivity implements TextWatcher {

    private AppBarLayout appBar;
    private JJUEditText airportEditText;
    private RecyclerView airportRecyclerView;
    private JJUTextView emptyTextView;
    private ProgressBar progressBar;

    private JJBTAirportAdapter adapter;

    private JJBTSearchAirportController controller;

    private JJBTAirportListener listener = new JJBTAirportListener() {
        @Override
        public void airportSelected(int position) {
            if(isControllerNotNull()){
                controller.airportSelected(position);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_airport);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        airportEditText = (JJUEditText) findViewById(R.id.search_airport_edit_text);
        airportRecyclerView = (RecyclerView) findViewById(R.id.search_airport_recycle_view);
        emptyTextView = (JJUTextView) findViewById(R.id.search_airport_empty_text_view);
        progressBar = (ProgressBar) findViewById(R.id.search_airport_progress_bar);
    }

    private void initiateDefaultValue() {
        controller = new JJBTSearchAirportController(this);
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(getString(R.string.search_airport));
        airportEditText.addTextChangedListener(this);
    }

    public void configureRecyclerView(List<JJBTAirportModel> models){
        adapter = new JJBTAirportAdapter(this,models,listener);
        airportRecyclerView.setAdapter(adapter);
        airportRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        decoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.icon_list_view_devider));
        airportRecyclerView.addItemDecoration(decoration);
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public JJBTAirportAdapter getAdapter() {
        return adapter;
    }

    public JJUTextView getEmptyTextView() {
        return emptyTextView;
    }

    public JJUEditText getAirportEditText() {
        return airportEditText;
    }

    private boolean isControllerNotNull(){
        return controller != null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable text) {
        if(isControllerNotNull()){
            controller.afterTextChanged(text.toString());
        }
    }
}
