package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.content.Intent;
import android.widget.Toast;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.activity.JJBTGetHotelActivity;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSearchRoomActivity;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUCalendarPickerActivity;
import com.jojonomic.jojoutilitieslib.utilities.JJUConstant;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Asus on 01/03/2017.
 */
public class JJBTSearchRoomController {

    private static final int REQ_CALENDAR_PICKER_FOR_CHECK_IN = 1;
    private static final int REQ_CALENDAR_PICKER_FOR_CHECK_OUT = 2;
    private static final int EXTRA_MONTH = 1;

    private Calendar checkInCalendar;
    private Calendar checkOutCalendar;

    private JJBTSearchRoomActivity activity;

    public JJBTSearchRoomController(JJBTSearchRoomActivity activity) {
        this.activity = activity;
        checkInCalendar = new GregorianCalendar();
        checkOutCalendar = new GregorianCalendar();
    }

    public void onClick(int id) {
        switch (id){
            case R.id.toolbar_back_image_button:
                activity.onBackPressed();
                break;
            case R.id.toolbar_submit_image_button:
                //TODO plane was click
                break;
            case R.id.search_room_button:
                String destination = activity.getDestinationEditText().getText().toString();
                int guest = 0;
                String totalGuest = activity.getGuestEditText().getText().toString();
                if(!totalGuest.isEmpty())guest = Integer.valueOf(totalGuest);
                int rooms = 0;
                String totalRoom = activity.getRoomsEditText().getText().toString();
                if(!totalRoom.isEmpty())rooms = Integer.valueOf(totalRoom);
                String description = activity.getDescriptionEditText().getText().toString();
                Toast.makeText(activity,"Destination: "+destination+
                        "\nGuest: "+guest+", Rooms: "+rooms+
                        "\nDescription: "+description,Toast.LENGTH_SHORT).show();
                activity.startActivity(new Intent(activity, JJBTGetHotelActivity.class));
                break;
            case R.id.search_room_check_in_container_linear_layout:
                activity.startActivityForResult(new Intent(activity, JJUCalendarPickerActivity.class), REQ_CALENDAR_PICKER_FOR_CHECK_IN);
                break;
            case R.id.search_room_check_out_container_linear_layout:
                activity.startActivityForResult(new Intent(activity, JJUCalendarPickerActivity.class), REQ_CALENDAR_PICKER_FOR_CHECK_OUT);
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == JJUConstant.RESULT_CODE_DATE_PICKER){
            long date = data.getLongExtra(JJUConstant.EXTRA_KEY_DATA,-1);
            if(date == -1)return;
            switch (requestCode){
                case REQ_CALENDAR_PICKER_FOR_CHECK_IN:
                    checkInCalendar.setTime(new Date(date));
                    activity.getCheckInTextView().setText(checkInCalendar.get(Calendar.DAY_OF_MONTH)+
                            "/"+(checkInCalendar.get(Calendar.MONTH)+EXTRA_MONTH)+"/"+checkInCalendar.get(Calendar.YEAR));
                    break;
                case REQ_CALENDAR_PICKER_FOR_CHECK_OUT:
                    checkOutCalendar.setTime(new Date(date));
                    activity.getCheckOutTextView().setText(checkOutCalendar.get(Calendar.DAY_OF_MONTH)+
                            "/"+(checkOutCalendar.get(Calendar.MONTH)+EXTRA_MONTH)+"/"+checkOutCalendar.get(Calendar.YEAR));

                    break;
            }
        }
    }
}
