package com.jojonomic.jojobiztrip.screen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTBookFlightController;
import com.jojonomic.jojobiztrip.support.adapter.JJBTBookFlightAdapter;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTBookFlightListener;
import com.jojonomic.jojobiztrip.support.dialog.JJBTSortAlertDialog;
import com.jojonomic.jojobiztrip.support.dialog.listener.JJBTSortAlertDialogListener;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUBaseActivity;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUButton;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJURectangleImageView;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

import java.util.List;


public class JJBTBookFlightActivity extends JJUBaseActivity
        implements View.OnClickListener, JJBTSortAlertDialogListener {

    private AppBarLayout appBar;

    private JJURectangleImageView originIconImageView;
    private JJUTextView originTitleTextView;
    private View originTabIndicatorView;
    private JJURectangleImageView destinationIconImageView;
    private JJUTextView destinationTitleTextView;
    private View destinationTabIndicatorView;
    private ProgressBar progressBar;
    private RecyclerView flightsRecyclerView;
    private JJUButton sortButton;
    private JJUButton filterButton;
    private JJUButton changeDateButton;
    private LinearLayout tabDestinationLinearLayout;
    private JJUTextView emptyTextView;

    private JJBTBookFlightAdapter flightsAdapter;

    private JJBTSortAlertDialog sortAlertDialog;

    private JJBTBookFlightController controller;

    private JJBTBookFlightListener listener = new JJBTBookFlightListener() {
        @Override
        public void onFlightChoose(int position) {
            if(isControllerNotNull()){
                controller.onFlightChoose(position);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_flight);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        originIconImageView = (JJURectangleImageView) findViewById(R.id.book_flight_origin_icon_image_view);
        originTitleTextView = (JJUTextView) findViewById(R.id.book_flight_origin_title_text_view);
        originTabIndicatorView = findViewById(R.id.book_flight_origin_tab_indicator_view);
        destinationIconImageView = (JJURectangleImageView) findViewById(R.id.book_flight_destination_icon_image_view);
        destinationTitleTextView = (JJUTextView) findViewById(R.id.book_flight_destination_title_text_view);
        destinationTabIndicatorView = findViewById(R.id.book_flight_destination_tab_indicator_view);
        progressBar = (ProgressBar) findViewById(R.id.book_flight_progress_bar);
        flightsRecyclerView = (RecyclerView) findViewById(R.id.book_flight_flights_list_view);
        sortButton = (JJUButton) findViewById(R.id.book_flight_sort_button);
        filterButton = (JJUButton) findViewById(R.id.book_flight_filter_button);
        changeDateButton = (JJUButton) findViewById(R.id.book_flight_change_date_button);
        tabDestinationLinearLayout = (LinearLayout) findViewById(R.id.book_flight_tab_destination_linear_layout);
        emptyTextView = (JJUTextView) findViewById(R.id.book_flight_empty_text_view);

        sortAlertDialog = new JJBTSortAlertDialog(this);
    }

    private void initiateDefaultValue() {
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.book_flight);
        appBar.findViewById(R.id.toolbar_back_image_button).setOnClickListener(this);
        appBar.findViewById(R.id.toolbar_submit_image_button).setOnClickListener(this);
        ((ImageButton)appBar.findViewById(R.id.toolbar_submit_image_button))
                .setImageResource(R.drawable.ic_search_white);

        sortButton.setOnClickListener(this);
        filterButton.setOnClickListener(this);
        changeDateButton.setOnClickListener(this);
        sortAlertDialog.setListener(this);
        controller = new JJBTBookFlightController(this);
    }

    public void configureRcycleView(List<JJBTFlightModel> flights){
        flightsAdapter = new JJBTBookFlightAdapter(flights, this, listener);
        flightsRecyclerView.setAdapter(flightsAdapter);
        flightsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        decoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.icon_list_view_devider));
        flightsRecyclerView.addItemDecoration(decoration);
    }

    public JJURectangleImageView getOriginIconImageView() {
        return originIconImageView;
    }

    public JJUTextView getOriginTitleTextView() {
        return originTitleTextView;
    }

    public View getOriginTabIndicatorView() {
        return originTabIndicatorView;
    }

    public JJURectangleImageView getDestinationIconImageView() {
        return destinationIconImageView;
    }

    public JJUTextView getDestinationTitleTextView() {
        return destinationTitleTextView;
    }

    public View getDestinationTabIndicatorView() {
        return destinationTabIndicatorView;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public RecyclerView getFlightsRecyclerView() {
        return flightsRecyclerView;
    }

    public JJUButton getSortButton() {
        return sortButton;
    }

    public JJUButton getFilterButton() {
        return filterButton;
    }

    public JJUButton getChangeDateButton() {
        return changeDateButton;
    }

    public LinearLayout getTabDestinationLinearLayout() {
        return tabDestinationLinearLayout;
    }

    public JJBTSortAlertDialog getSortAlertDialog() {
        return sortAlertDialog;
    }

    public JJUTextView getEmptyTextView() {
        return emptyTextView;
    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

    public JJBTBookFlightAdapter getFlightsAdapter() {
        return flightsAdapter;
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }

    @Override
    public void onBackPressed() {
        if(isControllerNotNull()){
            controller.onBaskPressed();
        }
    }

    @Override
    public void onSortedBy(int idRadioButton) {
        if(isControllerNotNull()){
            controller.onSortedBy(idRadioButton);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(isControllerNotNull()){
            controller.onActivityResult(requestCode, resultCode, data);
        }
    }
}
