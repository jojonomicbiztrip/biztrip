package com.jojonomic.jojobiztrip.screen.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.fragment.controller.JJBTMainController;
import com.jojonomic.jojoutilitieslib.screen.fragment.JJUBaseFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class JJBTMainFragment extends JJUBaseFragment implements View.OnClickListener{

    private RelativeLayout bookFlightRelativeLayout;
    private RelativeLayout getHotelsRecyclerLayout;
    private RelativeLayout transportRecyclerLayout;
    private JJBTMainController controller;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        loadView(view);
        initiateDefaultValue();
        return view;
    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

    private void loadView(View view) {
        bookFlightRelativeLayout = (RelativeLayout) view.findViewById(R.id.main_book_flight_relative_layout);
        getHotelsRecyclerLayout = (RelativeLayout) view.findViewById(R.id.main_get_hotels_relative_layout);
        transportRecyclerLayout = (RelativeLayout) view.findViewById(R.id.main_transport_relative_layout);
    }

    private void initiateDefaultValue() {
        controller = new JJBTMainController(this);
        bookFlightRelativeLayout.setOnClickListener(this);
        getHotelsRecyclerLayout.setOnClickListener(this);
        transportRecyclerLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()) {
            controller.onClick(v.getId());
        }
    }
}
