package com.jojonomic.jojobiztrip.screen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTSearchRoomController;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUEditText;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

public class JJBTSearchRoomActivity extends AppCompatActivity implements View.OnClickListener{

    private AppBarLayout appBar;
    private JJUEditText destinationEditText;
    private LinearLayout searchRoomButton;
    private LinearLayout checkInContainerLinearLayout;
    private LinearLayout checkOutContainerLinearLayout;
    private JJUTextView checkInTextView;
    private JJUTextView checkOutTextView;
    private JJUEditText guestEditText;
    private JJUEditText roomsEditText;
    private JJUEditText descriptionEditText;

    private JJBTSearchRoomController controller;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_room);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        destinationEditText = (JJUEditText) findViewById(R.id.search_room_destination_edit_text);
        searchRoomButton = (LinearLayout) findViewById(R.id.search_room_button);
        checkInTextView = (JJUTextView) findViewById(R.id.search_room_check_in_text_view);
        checkOutTextView = (JJUTextView) findViewById(R.id.search_room_check_out_text_view);
        checkInContainerLinearLayout = (LinearLayout) findViewById(R.id.search_room_check_in_container_linear_layout);
        checkOutContainerLinearLayout = (LinearLayout) findViewById(R.id.search_room_check_out_container_linear_layout);
        guestEditText = (JJUEditText) findViewById(R.id.search_room_guest_edit_text);
        roomsEditText = (JJUEditText) findViewById(R.id.search_room_rooms_edit_text);
        descriptionEditText = (JJUEditText) findViewById(R.id.search_room_description_edit_text);
    }

    private void initiateDefaultValue() {
        controller = new JJBTSearchRoomController(this);
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.trip_booking);
        appBar.findViewById(R.id.toolbar_back_image_button).setOnClickListener(this);
        appBar.findViewById(R.id.toolbar_submit_image_button).setOnClickListener(this);

        searchRoomButton.setOnClickListener(this);
        checkInContainerLinearLayout.setOnClickListener(this);
        checkOutContainerLinearLayout.setOnClickListener(this);
    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

    public JJUEditText getDestinationEditText() {
        return destinationEditText;
    }

    public JJUTextView getCheckInTextView() {
        return checkInTextView;
    }

    public JJUTextView getCheckOutTextView() {
        return checkOutTextView;
    }

    public JJUEditText getGuestEditText() {
        return guestEditText;
    }

    public JJUEditText getRoomsEditText() {
        return roomsEditText;
    }

    public JJUEditText getDescriptionEditText() {
        return descriptionEditText;
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(isControllerNotNull()){
            controller.onActivityResult(requestCode, resultCode, data);
        }
    }

}
