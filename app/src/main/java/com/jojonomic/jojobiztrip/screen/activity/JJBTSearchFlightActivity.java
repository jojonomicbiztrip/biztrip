package com.jojonomic.jojobiztrip.screen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTSearchFlightController;
import com.jojonomic.jojobiztrip.support.adapter.JJBTAirportAdapter;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUBaseActivity;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUEditText;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJURectangleImageView;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

import java.util.List;

public class JJBTSearchFlightActivity extends JJUBaseActivity
        implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, View.OnFocusChangeListener {

    private AppBarLayout appBar;
    private JJUEditText originEditText;
    private JJUEditText destinationEditText;
    private JJURectangleImageView originDestinationChangeButton;
    private Switch roundTripSwitch;
    private JJUTextView departureDateTextView;
    private JJUTextView returnDateTextView;
    private LinearLayout departureDateContainerLinearLayout;
    private LinearLayout returnDateContainerLinearLayout;
    private JJUEditText adultEditText;
    private JJUEditText childEditText;
    private JJUEditText infantEditText;
    private JJUEditText descriptionEditText;
    private LinearLayout searchFlightButton;


    private JJBTSearchFlightController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_flight);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        originEditText = (JJUEditText) findViewById(R.id.search_flight_origin_edit_text);
        destinationEditText = (JJUEditText) findViewById(R.id.search_flight_destination_edit_text);
        originDestinationChangeButton = (JJURectangleImageView) findViewById(R.id.search_flight_origin_destination_change_button);
        roundTripSwitch = (Switch) findViewById(R.id.search_flight_round_trip_switch);
        departureDateTextView = (JJUTextView) findViewById(R.id.search_flight_departure_date_text_view);
        returnDateTextView = (JJUTextView) findViewById(R.id.search_flight_return_date_text_view);
        departureDateContainerLinearLayout = (LinearLayout) findViewById(R.id.search_flight_departure_date_container_linear_layout);
        returnDateContainerLinearLayout = (LinearLayout) findViewById(R.id.search_flight_return_date_container_linear_layout);
        adultEditText = (JJUEditText) findViewById(R.id.search_flight_adult_edit_text);
        childEditText = (JJUEditText) findViewById(R.id.search_flight_child_edit_text);
        infantEditText = (JJUEditText) findViewById(R.id.search_flight_infant_edit_text);
        descriptionEditText = (JJUEditText) findViewById(R.id.search_flight_description_edit_text);
        searchFlightButton = (LinearLayout) findViewById(R.id.search_flight_button);

    }

    private void initiateDefaultValue() {

        controller = new JJBTSearchFlightController(this);
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.trip_booking);
        appBar.findViewById(R.id.toolbar_back_image_button).setOnClickListener(this);
        appBar.findViewById(R.id.toolbar_submit_image_button).setOnClickListener(this);

        originDestinationChangeButton.setOnClickListener(this);
        roundTripSwitch.setOnCheckedChangeListener(this);
        searchFlightButton.setOnClickListener(this);
        departureDateContainerLinearLayout.setOnClickListener(this);
        returnDateContainerLinearLayout.setOnClickListener(this);

        originEditText.setOnFocusChangeListener(this);
        destinationEditText.setOnFocusChangeListener(this);

        originEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isControllerNotNull()){
                    controller.onTextChanged(originEditText.getId(),s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        destinationEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isControllerNotNull()){
                    controller.onTextChanged(destinationEditText.getId(),s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

    public JJUEditText getOriginEditText() {
        return originEditText;
    }

    public JJUEditText getDestinationEditText() {
        return destinationEditText;
    }

    public JJUEditText getDescriptionEditText() {
        return descriptionEditText;
    }

    public LinearLayout getReturnDateContainerLinearLayout() {
        return returnDateContainerLinearLayout;
    }

    public JJUEditText getAdultEditText() {
        return adultEditText;
    }

    public JJUEditText getChildEditText() {
        return childEditText;
    }

    public JJUEditText getInfantEditText() {
        return infantEditText;
    }

    public TextView getReturnDateTextView() {
        return returnDateTextView;
    }

    public TextView getDepartureDateTextView() {
        return departureDateTextView;
    }

    public Switch getRoundTripSwitch() {
        return roundTripSwitch;
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isControllerNotNull()){
            controller.onCheckedChanged(buttonView.getId(), isChecked);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(isControllerNotNull()){
            controller.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus && isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }

}
