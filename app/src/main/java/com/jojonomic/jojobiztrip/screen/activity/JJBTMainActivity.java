package com.jojonomic.jojobiztrip.screen.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;


import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.fragment.JJBTMainFragment;

import com.jojonomic.jojoutilitieslib.screen.activity.JJUBaseActivity;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

public class JJBTMainActivity extends JJUBaseActivity {

    private AppBarLayout appBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
    }

    private void initiateDefaultValue() {
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        appBar.findViewById(R.id.toolbar_title_text_view);
        ((ImageButton)appBar.findViewById(R.id.toolbar_back_image_button)).setImageResource(R.drawable.ic_telegram);
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.trip_booking);
        getSupportFragmentManager().beginTransaction().add(R.id.main_container_frame_layout, new JJBTMainFragment()).commit();
    }

}
