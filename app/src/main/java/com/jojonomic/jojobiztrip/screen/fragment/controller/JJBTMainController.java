package com.jojonomic.jojobiztrip.screen.fragment.controller;

import android.content.Intent;
import android.widget.Toast;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSearchFlightActivity;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSearchRoomActivity;
import com.jojonomic.jojobiztrip.screen.fragment.JJBTMainFragment;

/**
 * Created by Asus on 22/02/2017.
 */
public class JJBTMainController {

    private JJBTMainFragment fragment;

    public JJBTMainController(JJBTMainFragment fragment) {
        this.fragment = fragment;
    }

    public void onClick(int id) {
        switch (id){
            case R.id.main_book_flight_relative_layout:
                fragment.getActivity()
                        .startActivity(
                                new Intent(fragment.getContext(), JJBTSearchFlightActivity.class));
                break;
            case R.id.main_get_hotels_relative_layout:
                fragment.getActivity()
                        .startActivity(
                                new Intent(fragment.getContext(), JJBTSearchRoomActivity.class));
                break;
            case R.id.main_transport_relative_layout:
                //TODO go to page TRANSPORT
                Toast.makeText(fragment.getContext(),"You click Transport",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
