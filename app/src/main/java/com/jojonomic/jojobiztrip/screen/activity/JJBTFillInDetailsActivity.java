package com.jojonomic.jojobiztrip.screen.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.model.JJBTContactModel;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTFillInDetailsController;
import com.jojonomic.jojobiztrip.support.adapter.JJBTFillInDetailsAdapter;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTHeaderFillInDetailsListener;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTPassengerFillInDetailsListener;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUBaseActivity;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

import java.util.Calendar;
import java.util.List;

public class JJBTFillInDetailsActivity extends JJUBaseActivity
        implements View.OnClickListener {

    private AppBarLayout appBar;
    private LinearLayout continueToPaymentButton;
    private RecyclerView fillInDetailsRecyclerView;
    private JJBTFillInDetailsAdapter adapter;

    private JJBTFillInDetailsController controller;

    private JJBTHeaderFillInDetailsListener headerListener = new JJBTHeaderFillInDetailsListener() {
        @Override
        public void addPassenger() {
            startActivityForResult(new Intent(JJBTFillInDetailsActivity.this, JJBTSetPassengerDetailsActivity.class)
                    ,JJBTFillInDetailsController.REQ_CREATE_PASSENGER_DETAILS);
        }
    };

    private JJBTPassengerFillInDetailsListener passengerListener = new JJBTPassengerFillInDetailsListener() {
        @Override
        public void onChangePassenger(int index) {
            if(isControllerNotNull()){
                controller.onChangePassenger(index);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_in_details);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        continueToPaymentButton = (LinearLayout) findViewById(R.id.fill_in_details_continue_to_payment_button);
        fillInDetailsRecyclerView = (RecyclerView) findViewById(R.id.fill_in_details_recycle_view);
    }

    private void initiateDefaultValue() {
        controller = new JJBTFillInDetailsController(this);
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.fill_in_details);
        appBar.findViewById(R.id.toolbar_back_image_button).setOnClickListener(this);
        appBar.findViewById(R.id.toolbar_submit_image_button).setOnClickListener(this);

    }

    public void configureRecycleView(JJBTContactModel contactModel
            , boolean isRoundTrip
            , JJBTAirportModel originAirport
            , JJBTAirportModel destinationAirport
            , JJBTFlightModel departFlight
            , JJBTFlightModel returnFlight
            , Calendar departureDate
            , Calendar returnDate
            , List<String> passenger){
        adapter = new JJBTFillInDetailsAdapter(this
                ,headerListener
                ,passengerListener
                ,contactModel
                ,isRoundTrip
                , originAirport
                , destinationAirport
                , departFlight
                , returnFlight
                , departureDate
                , returnDate
                ,passenger);
        fillInDetailsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        fillInDetailsRecyclerView.setAdapter(adapter);
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        decoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.icon_list_view_devider));
        fillInDetailsRecyclerView.addItemDecoration(decoration);
        continueToPaymentButton.setOnClickListener(this);
    }

    public JJBTFillInDetailsAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(isControllerNotNull()){
            controller.onActivityResult(requestCode, resultCode, data);
        }
    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

}
