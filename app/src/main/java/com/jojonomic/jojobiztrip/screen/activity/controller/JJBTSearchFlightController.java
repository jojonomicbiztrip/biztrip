package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.content.Intent;
import android.text.Editable;
import android.view.View;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.manager.connection.JJBTAirportConnectionManager;
import com.jojonomic.jojobiztrip.manager.connection.listener.JJBTAirportRequestListener;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.screen.activity.JJBTBookFlightActivity;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSearchAirportActivity;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSearchFlightActivity;
import com.jojonomic.jojobiztrip.utilities.JJBTConstant;
import com.jojonomic.jojobiztrip.utilities.helper.JJBTAirportHelper;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUCalendarPickerActivity;
import com.jojonomic.jojoutilitieslib.utilities.JJUConstant;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Asus on 23/02/2017.
 */
public class JJBTSearchFlightController {

    private static final int REQ_CALENDAR_PICKER_FOR_DEPARTURE = 0;
    private static final int REQ_CALENDAR_PICKER_FOR_RETURN = 1;
    private static final int REQ_ORIGIN = 2;
    private static final int REQ_DESTINATION = 3;
    private static final int EXTRA_MONTH = 1;
    private JJBTSearchFlightActivity activity;
    private JJBTAirportModel originAirport;
    private JJBTAirportModel destinationAirport;
    private Calendar departureDate;
    private Calendar returnDate;

    private boolean isAirportCommingFromSearchAirportActivity;

    public JJBTSearchFlightController(final JJBTSearchFlightActivity activity) {
        this.activity = activity;
        this.departureDate = Calendar.getInstance();
        this.returnDate = Calendar.getInstance();
        setUpCalendar();
    }

    private void setUpCalendar() {
        activity.getDepartureDateTextView().setText(departureDate.get(Calendar.DAY_OF_MONTH)+
                "/"+(departureDate.get(Calendar.MONTH)+EXTRA_MONTH)+"/"+departureDate.get(Calendar.YEAR));
        activity.getReturnDateTextView().setText(returnDate.get(Calendar.DAY_OF_MONTH)+
                "/"+(returnDate.get(Calendar.MONTH)+EXTRA_MONTH)+"/"+returnDate.get(Calendar.YEAR));
    }

    public void onClick(int id) {
        switch (id){
            case R.id.toolbar_back_image_button:
                activity.onBackPressed();
                break;
            case R.id.toolbar_submit_image_button:
                //TODO plane was click
                break;
            case R.id.search_flight_origin_edit_text:
                activity.startActivityForResult(new Intent(activity, JJBTSearchAirportActivity.class), REQ_ORIGIN);
                break;
            case R.id.search_flight_destination_edit_text:
                activity.startActivityForResult(new Intent(activity, JJBTSearchAirportActivity.class), REQ_DESTINATION);
                break;
            case R.id.search_flight_origin_destination_change_button:
                Editable temp = activity.getOriginEditText().getText();
                activity.getOriginEditText().setText(activity.getDestinationEditText().getText());
                activity.getDestinationEditText().setText(temp);
                break;
            case R.id.search_flight_departure_date_container_linear_layout:
                activity.startActivityForResult(new Intent(activity, JJUCalendarPickerActivity.class), REQ_CALENDAR_PICKER_FOR_DEPARTURE);
                break;
            case R.id.search_flight_return_date_container_linear_layout:
                activity.startActivityForResult(new Intent(activity, JJUCalendarPickerActivity.class), REQ_CALENDAR_PICKER_FOR_RETURN);
                break;
            case R.id.search_flight_button:
                //TODO check user input

                int adult = 0;
                String adultValue = activity.getAdultEditText().getText().toString();
                if(!adultValue.isEmpty())adult = Integer.valueOf(adultValue);

                int child = 0;
                String childValue = activity.getChildEditText().getText().toString();
                if(!childValue.isEmpty())child = Integer.valueOf(childValue);

                int infant = 0;
                String infantValue = activity.getInfantEditText().getText().toString();
                if(!infantValue.isEmpty())infant = Integer.valueOf(infantValue);

                if(!validationUserInput()){
                    return;
                }

                String description = activity.getDescriptionEditText().getText().toString();

                Intent intentToBookFlight = new Intent(activity, JJBTBookFlightActivity.class);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_ORIGIN_AIRPORT, originAirport);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_DESTINATION_AIRPORT, destinationAirport);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_IS_ROUND_TRIP, activity.getRoundTripSwitch().isChecked());
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_DEPARTURE_DATE, departureDate);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_RETURN_DATE, returnDate);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_TOTAL_ADULT, adult);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_TOTAL_CHILD, child);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_TOTAL_INFANT, infant);
                intentToBookFlight.putExtra(JJBTConstant.EXTRA_KEY_DESCRIPTION, description);
                activity.startActivity(intentToBookFlight);
        }
    }

    private boolean validationUserInput() {
        boolean output = true;
        if(originAirport == null
                || !JJBTAirportHelper.getAirportVisibleFormatted(originAirport).equals(activity.getOriginEditText().getText().toString())){
            activity.getOriginEditText().setError("Please, choose the available airport");
            output = false;
        }
        if(destinationAirport == null
                || !JJBTAirportHelper.getAirportVisibleFormatted(destinationAirport).equals(activity.getDestinationEditText().getText().toString())){
            activity.getDestinationEditText().setError("Please, choose the available airport");
            output = false;
        }
        return output;
    }

    public void onCheckedChanged(int id, boolean isChecked) {
        if(isChecked){
            activity.getReturnDateContainerLinearLayout().setVisibility(View.VISIBLE);
        }else{
            activity.getReturnDateContainerLinearLayout().setVisibility(View.GONE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == JJUConstant.RESULT_CODE_DATE_PICKER){
            long date = data.getLongExtra(JJUConstant.EXTRA_KEY_DATA,-1);
            if(date == -1)return;
            switch (requestCode){
                case REQ_CALENDAR_PICKER_FOR_DEPARTURE:
                    departureDate = new GregorianCalendar();
                    departureDate.setTime(new Date(date));
                    activity.getDepartureDateTextView().setText(departureDate.get(Calendar.DAY_OF_MONTH)+
                            "/"+(departureDate.get(Calendar.MONTH)+EXTRA_MONTH)+"/"+departureDate.get(Calendar.YEAR));
                    break;
                case REQ_CALENDAR_PICKER_FOR_RETURN:
                    returnDate = new GregorianCalendar();
                    returnDate.setTime(new Date(date));
                    activity.getReturnDateTextView().setText(returnDate.get(Calendar.DAY_OF_MONTH)+
                            "/"+(returnDate.get(Calendar.MONTH)+EXTRA_MONTH)+"/"+returnDate.get(Calendar.YEAR));
                    break;
            }
        }else if(resultCode == JJBTConstant.RESULT_CODE_AIRPORT){
            isAirportCommingFromSearchAirportActivity = true;
            JJBTAirportModel airportModel = (JJBTAirportModel) data.getSerializableExtra(JJBTConstant.EXTRA_KEY_AIRPORT);
            switch (requestCode){
                case REQ_ORIGIN:
                    originAirport = airportModel;
                    if(originAirport!=null)activity.getOriginEditText().setText(JJBTAirportHelper.getAirportVisibleFormatted(originAirport));
                    else activity.getOriginEditText().setText("");
                    break;
                case REQ_DESTINATION:
                    destinationAirport = airportModel;
                    if(destinationAirport!=null)activity.getDestinationEditText().setText(JJBTAirportHelper.getAirportVisibleFormatted(destinationAirport));
                    else activity.getDestinationEditText().setText("");
                    break;
            }
            isAirportCommingFromSearchAirportActivity = false;
        }
    }

    public void onTextChanged(int id, CharSequence s) {
        if(isAirportCommingFromSearchAirportActivity)return;
        Intent intent = new Intent(activity, JJBTSearchAirportActivity.class);
        intent.putExtra(JJBTConstant.EXTRA_KEY_AIRPORT_QUERY,s.toString());
        switch (id){
            case R.id.search_flight_origin_edit_text:
                activity.startActivityForResult(intent, REQ_ORIGIN);
                break;
            case R.id.search_flight_destination_edit_text:
                activity.startActivityForResult(intent, REQ_DESTINATION);
                break;
        }
    }
}
