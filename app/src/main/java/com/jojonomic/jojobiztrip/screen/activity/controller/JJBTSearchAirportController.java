package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.content.Intent;
import android.text.Editable;
import android.view.View;

import com.jojonomic.jojobiztrip.manager.connection.JJBTAirportConnectionManager;
import com.jojonomic.jojobiztrip.manager.connection.listener.JJBTAirportRequestListener;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSearchAirportActivity;
import com.jojonomic.jojobiztrip.utilities.JJBTConstant;
import com.jojonomic.jojobiztrip.utilities.helper.JJBTAirportHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Muhammad Umar Farisi
 * @created 17/04/2017
 */
public class JJBTSearchAirportController implements JJBTAirportRequestListener {

    private JJBTSearchAirportActivity activity;
    private List<JJBTAirportModel> realModels;
    private List<JJBTAirportModel> adapterModels;

    public JJBTSearchAirportController(JJBTSearchAirportActivity activity) {
        this.activity = activity;
        this.activity.getProgressBar().setVisibility(View.VISIBLE);
        JJBTAirportConnectionManager.getSingleton(activity).requestSearchAirport(this);
        loadIntent();
    }

    private void loadIntent() {
        String airportQuery = activity.getIntent().getStringExtra(JJBTConstant.EXTRA_KEY_AIRPORT_QUERY);
        if(airportQuery != null && !airportQuery.isEmpty())activity.getAirportEditText().setText(airportQuery);
    }

    @Override
    public void onRequestSuccess(List<JJBTAirportModel> models) {
        activity.getProgressBar().setVisibility(View.GONE);
        realModels = models;
        adapterModels = new ArrayList<>();
        if(activity.getAirportEditText().getText().toString().isEmpty()){
            adapterModels.addAll(realModels);
        }
        activity.configureRecyclerView(this.adapterModels);
        if(!activity.getAirportEditText().getText().toString().isEmpty()){
            filterAirport();
        }
        if(adapterModels.isEmpty())activity.getEmptyTextView().setVisibility(View.VISIBLE);
        else activity.getEmptyTextView().setVisibility(View.GONE);
    }

    @Override
    public void onRequestFailed(String message) {
        activity.getProgressBar().setVisibility(View.GONE);
        activity.getEmptyTextView().setVisibility(View.VISIBLE);
    }

    private void filterAirport() {
        adapterModels.clear();
        for(JJBTAirportModel model: realModels){
            if(JJBTAirportHelper.getAirportVisibleFormatted(model).toLowerCase()
                    .contains(activity.getAirportEditText().getText().toString().toLowerCase())){
                adapterModels.add(model);
            }
        }
        activity.getAdapter().notifyDataSetChanged();
    }

    public void airportSelected(int position) {
        Intent data = new Intent();
        data.putExtra(JJBTConstant.EXTRA_KEY_AIRPORT,adapterModels.get(position));
        activity.setResult(JJBTConstant.RESULT_CODE_AIRPORT,data);
        activity.finish();
    }

    public void afterTextChanged(String text) {
        if(!text.isEmpty()){
            filterAirport();
            if(adapterModels.isEmpty())activity.getEmptyTextView().setVisibility(View.VISIBLE);
            else activity.getEmptyTextView().setVisibility(View.GONE);
        }
    }
}
