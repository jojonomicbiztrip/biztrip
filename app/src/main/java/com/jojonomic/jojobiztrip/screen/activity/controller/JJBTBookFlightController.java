package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.manager.connection.JJBTFlightConnectionManager;
import com.jojonomic.jojobiztrip.manager.connection.listener.JJBTFlightRequestListener;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.screen.activity.JJBTBookFlightActivity;
import com.jojonomic.jojobiztrip.screen.activity.JJBTFillInDetailsActivity;
import com.jojonomic.jojobiztrip.screen.activity.JJBTFilterActivity;
import com.jojonomic.jojobiztrip.support.dialog.JJBTSortAlertDialog;
import com.jojonomic.jojobiztrip.utilities.JJBTConstant;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUCalendarPickerActivity;
import com.jojonomic.jojoutilitieslib.utilities.JJUConstant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Asus on 25/02/2017.
 */
public class JJBTBookFlightController implements JJBTFlightRequestListener {

    private static final int DEPART_FLIGHTS_PAGE = 0;
    private static final int RETURN_FLIGHTS_PAGE = 1;
    private static final int REQ_CHANGE_CALENDAR = 9;
    private static final int REQ_FILTER_FLIGHT = 10;

    private int currentPage;
    private List<JJBTFlightModel> departFlights;
    private List<JJBTFlightModel> returnsFlights;
    private List<JJBTFlightModel> flights;
    private JJBTFlightModel departFlight;
    private JJBTFlightModel returnFlight;
    private boolean isRoundTrip;

    private JJBTBookFlightActivity activity;
    private JJBTAirportModel originAirport;
    private JJBTAirportModel destinationAirport;
    private Calendar departureDate;
    private Calendar returnDate;
    private int totalAdult;
    private int totalChild;
    private int totalInfant;
    private String description;

    public JJBTBookFlightController(JJBTBookFlightActivity activity) {
        this.activity = activity;
        this.flights = new ArrayList<>();
        currentPage = DEPART_FLIGHTS_PAGE;
        originAirport = null;
        destinationAirport = null;
        departureDate = null;
        returnDate = null;
        description = null;
        loadIntent();
        configurePages();
        //show progress bar
        activity.getProgressBar().setVisibility(View.VISIBLE);
        if(isRoundTrip){
            JJBTFlightConnectionManager.getSingleton(activity)
                    .requestSearchFlight(originAirport.getAirportCode(), destinationAirport.getAirportCode()
                            ,departureDate.getTime(),returnDate.getTime(),totalAdult,totalChild,totalInfant,this);
        }else{
            JJBTFlightConnectionManager.getSingleton(activity)
                    .requestSearchFlight(originAirport.getAirportCode(), destinationAirport.getAirportCode()
                            ,departureDate.getTime(),null,totalAdult,totalChild,totalInfant,this);
        }
        activity.configureRcycleView(flights);
    }

    private void loadData(List<JJBTFlightModel> flights) {
        //hide empty if there's no data
        if(flights.isEmpty()){
            activity.getEmptyTextView().setVisibility(View.VISIBLE);
        }else{
            activity.getEmptyTextView().setVisibility(View.GONE);
        }
        //set data to adapter
        this.flights.clear();
        this.flights.addAll(flights);
        activity.getFlightsAdapter().notifyDataSetChanged();
    }

    private void loadIntent(){
        //TODO blom ada child, adult, infant yaitu passenger
        if(activity.getIntent() != null){
            originAirport = (JJBTAirportModel) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_ORIGIN_AIRPORT);
            destinationAirport = (JJBTAirportModel) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_DESTINATION_AIRPORT);
            isRoundTrip = (activity.getIntent().getBooleanExtra(JJBTConstant.EXTRA_KEY_IS_ROUND_TRIP,false));
            departureDate = (Calendar) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_DEPARTURE_DATE);
            returnDate = (Calendar) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_RETURN_DATE);
            totalAdult = activity.getIntent().getIntExtra(JJBTConstant.EXTRA_KEY_TOTAL_ADULT, 0);
            totalChild = activity.getIntent().getIntExtra(JJBTConstant.EXTRA_KEY_TOTAL_CHILD, 0);
            totalInfant = activity.getIntent().getIntExtra(JJBTConstant.EXTRA_KEY_TOTAL_INFANT, 0);
            description = activity.getIntent().getStringExtra(JJBTConstant.EXTRA_KEY_DESCRIPTION);
        }
    }

    private void configurePages(){
        activity.getOriginTitleTextView().setText(originAirport.getAirportCode()+"-"+destinationAirport.getAirportCode());
        activity.getDestinationTitleTextView().setText(destinationAirport.getAirportCode()+"-"+originAirport.getAirportCode());
        if(!isRoundTrip){
            activity.getTabDestinationLinearLayout().setVisibility(View.GONE);
        }
    }

    public void onClick(int id) {
        switch (id){
            case R.id.toolbar_back_image_button:
                onBaskPressed();
                break;
            case R.id.toolbar_submit_image_button:
                //TODO plane was click
                Toast.makeText(activity,"IMAGE RIGHT",Toast.LENGTH_SHORT).show();
                break;
            case R.id.book_flight_sort_button:
                activity.getSortAlertDialog().show();
                break;
            case R.id.book_flight_filter_button:
                activity.startActivityForResult(new Intent(activity, JJBTFilterActivity.class), REQ_FILTER_FLIGHT);
                break;
            case R.id.book_flight_change_date_button:
                activity.startActivityForResult(new Intent(activity, JJUCalendarPickerActivity.class), REQ_CHANGE_CALENDAR);
                break;
        }
    }

    public void onBaskPressed() {
        switch (currentPage){
            case DEPART_FLIGHTS_PAGE:
                activity.finish();
                break;
            case RETURN_FLIGHTS_PAGE:
                currentPage = DEPART_FLIGHTS_PAGE;
                loadData(departFlights);
                //change tab
                activity.getOriginTabIndicatorView().setBackgroundColor(ContextCompat.getColor(activity, R.color.dark_aqua));
                activity.getOriginTitleTextView().setTextColor(ContextCompat.getColor(activity, R.color.dark_aqua));
                activity.getDestinationTabIndicatorView().setBackgroundColor(ContextCompat.getColor(activity, R.color.light_gray));
                activity.getDestinationTitleTextView().setTextColor(ContextCompat.getColor(activity, R.color.dark_gray));

                break;
        }
    }

    public void onFlightChoose(int position) {
        //TODO sent total passenger
        switch (currentPage){
            case DEPART_FLIGHTS_PAGE:
                departFlight = departFlights.get(position);
                if(isRoundTrip) {
                    currentPage = RETURN_FLIGHTS_PAGE;
                    loadData(returnsFlights);
                    //change tab
                    activity.getOriginTabIndicatorView().setBackgroundColor(ContextCompat.getColor(activity, R.color.light_gray));
                    activity.getOriginTitleTextView().setTextColor(ContextCompat.getColor(activity, R.color.dark_gray));
                    activity.getDestinationTabIndicatorView().setBackgroundColor(ContextCompat.getColor(activity, R.color.dark_aqua));
                    activity.getDestinationTitleTextView().setTextColor(ContextCompat.getColor(activity, R.color.dark_aqua));
                }else{
                    goToFillInDetailsPage();
                }
                break;
            case RETURN_FLIGHTS_PAGE:
                returnFlight = returnsFlights.get(position);
                goToFillInDetailsPage();
                break;
        }
    }

    private void goToFillInDetailsPage(){
        Intent intent = new Intent(activity, JJBTFillInDetailsActivity.class);
        intent.putExtra(JJBTConstant.EXTRA_KEY_ORIGIN_AIRPORT,originAirport);
        intent.putExtra(JJBTConstant.EXTRA_KEY_DESTINATION_AIRPORT,destinationAirport);
        intent.putExtra(JJBTConstant.EXTRA_KEY_DEPARTURE_DATE,departureDate);
        intent.putExtra(JJBTConstant.EXTRA_KEY_RETURN_DATE,returnDate);
        intent.putExtra(JJBTConstant.EXTRA_KEY_IS_ROUND_TRIP, isRoundTrip);
        intent.putExtra(JJBTConstant.EXTRA_KEY_DEPART_FLIGHT, departFlight);
        intent.putExtra(JJBTConstant.EXTRA_KEY_RETURN_FLIGHT, returnFlight);
        intent.putExtra(JJBTConstant.EXTRA_KEY_TOTAL_ADULT, totalAdult);
        intent.putExtra(JJBTConstant.EXTRA_KEY_TOTAL_CHILD, totalChild);
        intent.putExtra(JJBTConstant.EXTRA_KEY_TOTAL_INFANT, totalInfant);
        activity.startActivity(intent);
    }

    public void onSortedBy(final int idRadioButton) {
        Collections.sort(flights, new Comparator<JJBTFlightModel>() {
            @Override
            public int compare(JJBTFlightModel left, JJBTFlightModel right) {
                switch (idRadioButton){
                    case JJBTSortAlertDialog.SORTED_BY_NAME_ID:
                        return left.getFlightAirlinesName().compareTo(right.getFlightAirlinesName());
                    case JJBTSortAlertDialog.SORTED_BY_PRICE_ID:
                        return (int)(left.getFlightRealPrice() - right.getFlightRealPrice());
                    case JJBTSortAlertDialog.SORTED_BY_TIME_ID:
                        return left.getFlightDuration() - right.getFlightDuration();
                }
                return 0;
            }
        });

        switch (currentPage){
            case DEPART_FLIGHTS_PAGE:
                departFlights.clear();
                departFlights.addAll(flights);
                break;
            case RETURN_FLIGHTS_PAGE:
                returnsFlights.clear();
                returnsFlights.addAll(flights);
                break;
        }

        activity.getFlightsAdapter().notifyDataSetChanged();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == JJUConstant.RESULT_CODE_DATE_PICKER){
            if(requestCode == REQ_CHANGE_CALENDAR){
                long date = data.getLongExtra(JJUConstant.EXTRA_KEY_DATA,-1);
                if(date == -1)return;
                Toast.makeText(activity,"You Have Select New Date: "+(new Date(date).toString()),Toast.LENGTH_SHORT).show();
            }
        }else if(resultCode == JJBTConstant.RESULT_CODE_FILTER_FLIGHT){
            if(requestCode == REQ_FILTER_FLIGHT){
                boolean isFilteredByTime = data.getBooleanExtra(JJBTConstant.EXTRA_KEY_IS_FILTERED_BY_TIME,false);
                boolean isFilteredByPrice = data.getBooleanExtra(JJBTConstant.EXTRA_KEY_IS_FILTERED_BY_PRICE,false);
                if(isFilteredByPrice && isFilteredByTime){
                    //TODO filtered by price and time
                }else if(isFilteredByPrice){
                    //TODO filtered by price only
                }else if(isFilteredByTime){
                    //TODO filtered by time only
                }
            }
        }
    }

    @Override
    public void onRequestSuccess(List<JJBTFlightModel> depart, List<JJBTFlightModel> returns) {
        activity.getProgressBar().setVisibility(View.GONE);
        departFlights = depart;
        returnsFlights = returns;
        loadData(departFlights);
    }

    @Override
    public void onRequestFailed(String message) {
        //TODO
        activity.getProgressBar().setVisibility(View.GONE);
    }
}
