package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.widget.Toast;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.activity.JJBTFilterActivity;

/**
 * @author Muhammad Umar Farisi
 * @created 15/03/2017
 */
public class JJBTFilterController {

    private static final Number MINIMUM_PRICE_VALUE = 0;
    private static final int MINIMUM_TIME_VALUE = 0;

    private JJBTFilterActivity activity;

    private Number lowerPrice;
    private Number higherPrice;

    public JJBTFilterController(JJBTFilterActivity activity) {
        this.activity = activity;

    }

    public void onClick(int id) {
        switch (id){
            case R.id.toolbar_back_image_button:
                activity.onBackPressed();
                break;
            case R.id.filter_button:
                //TODO
                String output = "Lower Price: "+lowerPrice
                        +"\nHigher Price: "+higherPrice
                        +"\nFrom Hour: "+activity.getFromHourNumberPicker().getNumber()
                        +"\nFrom Minute: "+activity.getFromMinuteNumberPicker().getNumber()
                        +"\nTo Hour: "+activity.getToHourNumberPicker().getNumber()
                        +"\nTo Minute: "+activity.getToMinuteNumberPicker().getNumber();
                Toast.makeText(activity,output,Toast.LENGTH_SHORT).show();
                break;
            case R.id.filter_refresh_filtered_by_price_button:
                activity.getLowerPriceEditText().getText().clear();
                activity.getHigherPriceEditText().getText().clear();
                lowerPrice = MINIMUM_PRICE_VALUE;
                higherPrice = MINIMUM_PRICE_VALUE;
                break;
            case R.id.filter_refresh_filtered_by_time_button:
                activity.getFromHourNumberPicker().setNumber(MINIMUM_TIME_VALUE);
                activity.getFromMinuteNumberPicker().setNumber(MINIMUM_TIME_VALUE);
                activity.getToHourNumberPicker().setNumber(MINIMUM_TIME_VALUE);
                activity.getToMinuteNumberPicker().setNumber(MINIMUM_TIME_VALUE);
                break;
        }
    }

    public void onLowerPriceChange(Number lowerPrice) {
        this.lowerPrice = lowerPrice;
    }

    public void onHigherPriceChange(Number higherPrice) {
        this.higherPrice = higherPrice;
    }
}
