package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.app.Activity;
import android.content.Intent;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSetPassengerDetailsActivity;
import com.jojonomic.jojobiztrip.utilities.JJBTConstant;

import java.util.StringTokenizer;

/**
 * Created by Asus on 08/03/2017.
 */
public class JJBTSetPassengerDetailController {

    private JJBTSetPassengerDetailsActivity activity;

    private boolean isChangeExistPassenger;
    private int index;
    private String oldPassengerName;

    public JJBTSetPassengerDetailController(JJBTSetPassengerDetailsActivity activity) {
        this.activity = activity;
        loadIntent();
        setUpData();
    }

    private void loadIntent(){
        isChangeExistPassenger = activity
                .getIntent()
                .getIntExtra(JJBTConstant.EXTRA_KEY_INDEX_OF_PASSENGER, -1) != -1;
        if(isChangeExistPassenger){
            index = activity
                    .getIntent()
                    .getIntExtra(JJBTConstant.EXTRA_KEY_INDEX_OF_PASSENGER, -1);
            oldPassengerName = activity
                    .getIntent()
                    .getStringExtra(JJBTConstant.EXTRA_KEY_PASSENGER_NAME);
        }
    }


    private void setUpData() {
        if(isChangeExistPassenger){
            StringTokenizer titleAndName = new StringTokenizer(oldPassengerName,".");
            String title = titleAndName.nextToken();
            String[] titles = activity.getResources().getStringArray(R.array.title_passenger);
            for(int i = 0 ; i < titles.length ; i++){
                if(titles[i].equals(title))activity.getTitleSpinner().setSelection(i);
            }
            activity.getNameEditText().setText(titleAndName.nextToken().trim());
        }
    }

    public void onClick(int id){
        switch (id) {
            case R.id.toolbar_back_image_button:
                activity.onBackPressed();
                break;
            case R.id.create_passenger_button:
                String name = activity.getNameEditText().getText().toString();
                String title = activity.getTitleSpinner().getSelectedItem().toString();
                Intent intent = new Intent();
                intent.putExtra(JJBTConstant.EXTRA_KEY_PASSENGER_NAME, name);
                intent.putExtra(JJBTConstant.EXTRA_KEY_PASSENGER_TITLE, title);
                if(isChangeExistPassenger){
                    intent.putExtra(JJBTConstant.EXTRA_KEY_INDEX_OF_PASSENGER, index);
                }
                activity.setResult(Activity.RESULT_OK,intent);
                activity.finish();
                break;
        }
    }

}
