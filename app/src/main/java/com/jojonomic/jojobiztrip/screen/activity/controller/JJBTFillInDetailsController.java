package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.model.JJBTContactModel;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.screen.activity.JJBTSetPassengerDetailsActivity;
import com.jojonomic.jojobiztrip.screen.activity.JJBTFillInDetailsActivity;
import com.jojonomic.jojobiztrip.utilities.JJBTConstant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Asus on 25/02/2017.
 */
public class JJBTFillInDetailsController {

    public static final int REQ_CREATE_PASSENGER_DETAILS = 1;
    public static final int REQ_CHANGE_PASSENGER_DETAILS = 2;

    private boolean isRoundTrip;
    private Calendar departureDate;
    private Calendar returnDate;
    private JJBTFlightModel departFlight;
    private JJBTFlightModel returnFlight;
    private int totalAdult;
    private int totalChild;
    private int totalInfant;
    private List<String> passenger;
    private JJBTContactModel contactModel;

    private JJBTFillInDetailsActivity activity;
    private JJBTAirportModel originAirport;
    private JJBTAirportModel destinationAirport;

    public JJBTFillInDetailsController(JJBTFillInDetailsActivity activity) {
        this.activity = activity;
        this.passenger = new ArrayList<>();
        this.contactModel = new JJBTContactModel();
        loadIntent();
        addDefaultPassenger();
        activity.configureRecycleView(contactModel
                , isRoundTrip
                , originAirport
                , destinationAirport
                , departFlight
                , returnFlight
                , departureDate
                , returnDate
                , passenger);
    }

    private void loadIntent(){
        isRoundTrip = false;
        if(activity.getIntent() != null){
            originAirport = (JJBTAirportModel) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_ORIGIN_AIRPORT);
            destinationAirport = (JJBTAirportModel) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_DESTINATION_AIRPORT);
            isRoundTrip = activity.getIntent().getBooleanExtra(JJBTConstant.EXTRA_KEY_IS_ROUND_TRIP,false);
            departureDate = (Calendar) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_DEPARTURE_DATE);
            returnDate = (Calendar) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_RETURN_DATE);
            departFlight = (JJBTFlightModel) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_DEPART_FLIGHT);
            returnFlight = (JJBTFlightModel) activity.getIntent().getSerializableExtra(JJBTConstant.EXTRA_KEY_RETURN_FLIGHT);
            totalAdult = activity.getIntent().getIntExtra(JJBTConstant.EXTRA_KEY_TOTAL_ADULT,0);
            totalChild = activity.getIntent().getIntExtra(JJBTConstant.EXTRA_KEY_TOTAL_CHILD,0);
            totalInfant = activity.getIntent().getIntExtra(JJBTConstant.EXTRA_KEY_TOTAL_INFANT,0);
        }
    }

    private void addDefaultPassenger() {
        for(int i = 0 ; i < totalAdult ; i++)passenger.add("Mr. Adult");
        for(int i = 0 ; i < totalChild ; i++)passenger.add("Mr. Child");
        for(int i = 0 ; i < totalInfant ; i++)passenger.add("Mr. Infant");
    }

    public void onClick(int id) {
        switch (id){
            case R.id.toolbar_back_image_button:
                activity.onBackPressed();
                break;
            case R.id.toolbar_submit_image_button:
                //TODO plane was click
                break;
            case R.id.fill_in_details_continue_to_payment_button:
                //TODO continue to payment
                Toast.makeText(activity,"Continue To Payment",Toast.LENGTH_SHORT).show();
                break;
            case R.id.fill_in_details_add_passenger_button:
                Intent intent = new Intent(activity, JJBTSetPassengerDetailsActivity.class);
                activity.startActivityForResult(intent,REQ_CREATE_PASSENGER_DETAILS);
                break;
        }
    }

    public void onChangePassenger(int index) {
        String name = passenger.get(index);
        Intent intent = new Intent(activity, JJBTSetPassengerDetailsActivity.class);
        intent.putExtra(JJBTConstant.EXTRA_KEY_PASSENGER_NAME, name);
        intent.putExtra(JJBTConstant.EXTRA_KEY_INDEX_OF_PASSENGER,index);
        activity.startActivityForResult(intent,REQ_CHANGE_PASSENGER_DETAILS);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK && data != null){
            String name = data.getStringExtra(JJBTConstant.EXTRA_KEY_PASSENGER_NAME);
            String title = data.getStringExtra(JJBTConstant.EXTRA_KEY_PASSENGER_TITLE);
            String titleAndName = title+". "+name;
            if(requestCode == REQ_CREATE_PASSENGER_DETAILS ){
                passenger.add(titleAndName);
            }else if(requestCode == REQ_CHANGE_PASSENGER_DETAILS){
                int index = data.getIntExtra(JJBTConstant.EXTRA_KEY_INDEX_OF_PASSENGER,-1);
                if(index != -1)passenger.set(index, titleAndName);
            }
            activity.getAdapter().notifyDataSetChanged();
        }
    }
}
