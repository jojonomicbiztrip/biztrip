package com.jojonomic.jojobiztrip.screen.activity.controller;

import android.view.View;
import android.widget.Toast;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTHotelModel;
import com.jojonomic.jojobiztrip.screen.activity.JJBTGetHotelActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Asus on 01/03/2017.
 */
public class JJBTGetHotelController {

    private JJBTGetHotelActivity activity;
    private List<JJBTHotelModel> hotels;

    public JJBTGetHotelController(JJBTGetHotelActivity activity) {
        this.activity = activity;
        hotels = new ArrayList<>();
        activity.configureRecyclerView(hotels);
        loadForFirstTimeData();
    }

    private void loadForFirstTimeData() {
        //show progress bar
        activity.getProgressBar().setVisibility(View.VISIBLE);
        //clear all data
        hotels.clear();

        //TODO change dummy
        for(int i=0 ; i< 50;i++){
            int rating = new Random().nextInt(6);
            hotels.add(new JJBTHotelModel("Hyatt International"+i,"Cihampelas"+i, rating,"IDR 1.570.000,-","IDR 2.000.000,-"));
        }

        //TODO get all data
        //TODO if data is 0 show progress bar and opposite
        //hide progress bar
        activity.getProgressBar().setVisibility(View.GONE);
        //hide empty if there's no data
        if(hotels.isEmpty()){
            activity.getEmptyTextView().setVisibility(View.VISIBLE);
        }else{
            activity.getEmptyTextView().setVisibility(View.GONE);
        }
        activity.getAdapter().notifyDataSetChanged();
    }

    public void onHotelChoose(int position) {
        //TODO
        Toast.makeText(activity,"POSITION: "+position,Toast.LENGTH_SHORT).show();
    }

    public void onClick(int id) {
        switch (id){
            case R.id.toolbar_back_image_button:
                activity.onBackPressed();
                break;
            case R.id.toolbar_submit_image_button:
                //TODO plane was click
                Toast.makeText(activity,"IMAGE RIGHT",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
