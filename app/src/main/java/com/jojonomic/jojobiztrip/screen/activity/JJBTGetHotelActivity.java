package com.jojonomic.jojobiztrip.screen.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.model.JJBTHotelModel;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTGetHotelController;
import com.jojonomic.jojobiztrip.support.adapter.JJBTGetHotelAdapter;
import com.jojonomic.jojobiztrip.support.adapter.listener.JJBTGetHotelListener;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

import java.util.List;

public class JJBTGetHotelActivity extends AppCompatActivity
        implements View.OnClickListener {

    private AppBarLayout appBar;
    private RecyclerView hotelRecyclerView;
    private ProgressBar progressBar;
    private JJUTextView emptyTextView;

    private JJBTGetHotelController controller;

    private JJBTGetHotelAdapter adapter;

    private JJBTGetHotelListener listener = new JJBTGetHotelListener() {
        @Override
        public void onHotelChoose(int position) {
            if(isControllerNotNull()){
                controller.onHotelChoose(position);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_hotel);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        hotelRecyclerView = (RecyclerView) findViewById(R.id.get_hotel_recycle_view);
        progressBar = (ProgressBar) findViewById(R.id.get_hotel_progress_bar);
        emptyTextView = (JJUTextView) findViewById(R.id.get_hotel_empty_text_view);
    }

    private void initiateDefaultValue() {
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.get_hotel);
        appBar.findViewById(R.id.toolbar_back_image_button).setOnClickListener(this);
        appBar.findViewById(R.id.toolbar_submit_image_button).setOnClickListener(this);
        ((ImageButton)appBar.findViewById(R.id.toolbar_submit_image_button))
                .setImageResource(R.drawable.ic_search_white);

        controller = new JJBTGetHotelController(this);

    }

    public void configureRecyclerView(List<JJBTHotelModel> hotels){
        adapter = new JJBTGetHotelAdapter(hotels, this, listener);
        hotelRecyclerView.setAdapter(adapter);
        hotelRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        decoration.setDrawable(ContextCompat.getDrawable(this,R.drawable.icon_list_view_devider));
        hotelRecyclerView.addItemDecoration(decoration);
    }

    public RecyclerView getHotelRecyclerView() {
        return hotelRecyclerView;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public JJUTextView getEmptyTextView() {
        return emptyTextView;
    }

    public JJBTGetHotelAdapter getAdapter() {
        return adapter;
    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }
}
