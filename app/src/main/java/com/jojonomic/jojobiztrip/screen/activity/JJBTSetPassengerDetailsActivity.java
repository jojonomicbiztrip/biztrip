package com.jojonomic.jojobiztrip.screen.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Spinner;

import com.jojonomic.jojobiztrip.R;
import com.jojonomic.jojobiztrip.screen.activity.controller.JJBTSetPassengerDetailController;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUBaseActivity;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUButton;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUEditText;
import com.jojonomic.jojoutilitieslib.support.extentions.view.JJUTextView;

public class JJBTSetPassengerDetailsActivity extends JJUBaseActivity implements View.OnClickListener{

    private AppBarLayout appBar;
    private JJUEditText nameEditText;
    private Spinner titleSpinner;
    private JJUButton createPassengerButton;

    private JJBTSetPassengerDetailController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_passenger_details);
        loadView();
        initiateDefaultValue();
    }

    private void loadView() {
        appBar = (AppBarLayout) findViewById(R.id.toolbar);
        nameEditText = (JJUEditText) findViewById(R.id.create_passenger_name_edit_text);
        titleSpinner = (Spinner) findViewById(R.id.create_passenger_title_spinner);
        createPassengerButton = (JJUButton) findViewById(R.id.create_passenger_button);
    }

    private void initiateDefaultValue() {
        setSupportActionBar((Toolbar) appBar.findViewById(R.id.main_toolbar));
        appBar.findViewById(R.id.toolbar_title_text_view);
        ((JJUTextView)appBar.findViewById(R.id.toolbar_title_text_view)).setText(R.string.passenger_details);
        appBar.findViewById(R.id.toolbar_back_image_button).setOnClickListener(this);

        createPassengerButton.setOnClickListener(this);

        controller = new JJBTSetPassengerDetailController(this);

    }

    public JJUEditText getNameEditText() {
        return nameEditText;
    }

    public Spinner getTitleSpinner() {
        return titleSpinner;
    }

    @Override
    public void onClick(View v) {
        if(isControllerNotNull()){
            controller.onClick(v.getId());
        }
    }

    private boolean isControllerNotNull() {
        return controller != null;
    }

}
