package com.jojonomic.jojobiztrip.manager.connection;

import android.content.Context;

import com.jojonomic.jojobiztrip.manager.connection.listener.JJBTAirportRequestListener;
import com.jojonomic.jojobiztrip.model.JJBTAirportModel;
import com.jojonomic.jojobiztrip.utilities.JJBTConstant;
import com.jojonomic.jojobiztrip.utilities.JJBTConstantConnection;
import com.jojonomic.jojoutilitieslib.manager.connection.JJUConnectionManager;
import com.jojonomic.jojoutilitieslib.manager.connection.listener.ConnectionManagerListener;
import com.jojonomic.jojoutilitieslib.model.JJUConnectionResultModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Muhammad Umar Farisi
 * @created 30/03/2017
 */
public class JJBTAirportConnectionManager extends JJUConnectionManager {

    private static JJBTAirportConnectionManager singleton;
    private Context context;

    public JJBTAirportConnectionManager(Context context){
        this.context = context;
    }

    public static JJBTAirportConnectionManager getSingleton(Context context) {
        if (singleton == null) {
            singleton = new JJBTAirportConnectionManager(context);
        }
        return singleton;
    }

    public void requestSearchAirport(final JJBTAirportRequestListener listener) {
        requestGET(JJBTConstantConnection.URL_SEARCH_AIRPORT, new ConnectionManagerListener() {

            List<JJBTAirportModel> airportModels = null;

            @Override
            public JJUConnectionResultModel onHandleSuccessData(String response) {
                JJUConnectionResultModel result = new JJUConnectionResultModel();
                try {
                    JSONObject responseJSONObject = new JSONObject(response);
                    int status = responseJSONObject
                            .getJSONObject(JJBTConstant.JSON_KEY_DIAGNOSTIC)
                            .getInt(JJBTConstant.JSON_KEY_STATUS);
                    if(status == 200){
                        airportModels = new ArrayList<>();
                        JSONArray airportJSONArray = responseJSONObject
                                .getJSONObject(JJBTConstant.JSON_KEY_ALL_AIRPORT)
                                .getJSONArray(JJBTConstant.JSON_KEY_AIRPORT);
                        for(int i = 0 ; i < airportJSONArray.length() ; i++){
                            JSONObject airportJSONObject = airportJSONArray.getJSONObject(i);
                            JJBTAirportModel airportModel = parseAirportFromJson(airportJSONObject);
                            airportModels.add(airportModel);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    String message = "Search Airport Failed";
                    result.setIsError(true);
                    result.setMessage(message);
                }
                return result;
            }

            @Override
            public JJUConnectionResultModel onHandleFailedData(String response) {
                String message = "Search Airport Failed";
                JJUConnectionResultModel result = new JJUConnectionResultModel(message, true);
                return result;
            }

            @Override
            public void onUpdateUI(JJUConnectionResultModel model) {
                    if (listener != null) {
                        if (model.isError() || airportModels == null) {
                            listener.onRequestFailed(model.getMessage());
                        }else {
                            listener.onRequestSuccess(airportModels);
                        }
                    }
            }
        });
    }

    private JJBTAirportModel parseAirportFromJson(JSONObject response) throws JSONException {
        String airportName = response.getString(JJBTConstant.JSON_KEY_AIRPORT_NAME);
        String airportCode = response.getString(JJBTConstant.JSON_KEY_AIRPORT_CODE);
        String locationName = response.getString(JJBTConstant.JSON_KEY_LOCATION_NAME);
        String countryId = response.getString(JJBTConstant.JSON_KEY_COUNTRY_ID);
        return new JJBTAirportModel(airportName,airportCode,locationName,countryId);
    }

}
