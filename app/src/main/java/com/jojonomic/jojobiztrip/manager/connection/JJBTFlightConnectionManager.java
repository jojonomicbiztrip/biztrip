package com.jojonomic.jojobiztrip.manager.connection;

import android.content.Context;

import com.jojonomic.jojobiztrip.manager.connection.listener.JJBTFlightRequestListener;
import com.jojonomic.jojobiztrip.model.JJBTFlightModel;
import com.jojonomic.jojobiztrip.utilities.JJBTConstant;
import com.jojonomic.jojobiztrip.utilities.JJBTConstantConnection;
import com.jojonomic.jojobiztrip.utilities.helper.JJBTDurationHelper;
import com.jojonomic.jojoutilitieslib.manager.connection.JJUConnectionManager;
import com.jojonomic.jojoutilitieslib.manager.connection.listener.ConnectionManagerListener;
import com.jojonomic.jojoutilitieslib.model.JJUConnectionResultModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Muhammad Umar Farisi
 * @created 07/04/2017
 */
public class JJBTFlightConnectionManager extends JJUConnectionManager {
    private static JJBTFlightConnectionManager singleton;
    private Context context;

    public JJBTFlightConnectionManager(Context context){
        this.context = context;
    }

    public static JJBTFlightConnectionManager getSingleton(Context context){
        if(singleton == null){
            singleton = new JJBTFlightConnectionManager(context);
        }
        return singleton;
    }

    public void requestSearchFlight(String departureAirportCode,
                                    String arrivalAirportCode,
                                    final Date departDate,
                                    final Date returnDate,
                                    int totalAdult,
                                    int totalChild,
                                    int totalInfant,
                                    final JJBTFlightRequestListener listener){
        try {
            JSONObject params = new JSONObject();
            params.put(JJBTConstant.JSON_KEY_DEPARTURE_AIRPORT_CODE,departureAirportCode);
            params.put(JJBTConstant.JSON_KEY_ARRIVAL_AIRPORT_CODE,arrivalAirportCode);
            params.put(JJBTConstant.JSON_KEY_DEPART_DATE,departDate.toString());
            params.put(JJBTConstant.JSON_KEY_TOTAL_ADULT,String.valueOf(totalAdult));
            params.put(JJBTConstant.JSON_KEY_TOTAL_CHILD,String.valueOf(totalChild));
            params.put(JJBTConstant.JSON_KEY_TOTAL_INFANT,String.valueOf(totalInfant));
            if(returnDate != null){
                params.put(JJBTConstant.JSON_KEY_RETURN_DATE,returnDate.toString());
            }
            requestPOST(JJBTConstantConnection.URL_SEARCH_FLIGHT, params.toString(), new ConnectionManagerListener() {

                List<JJBTFlightModel> departFlights = null;
                List<JJBTFlightModel> returnFlights = null;

                @Override
                public JJUConnectionResultModel onHandleSuccessData(String s) {
                    JJUConnectionResultModel result = new JJUConnectionResultModel();
                    try {
                        JSONObject responseJSONObject = new JSONObject(s);

                        JSONArray departFlightsJSONArray = responseJSONObject
                                .getJSONObject(JJBTConstant.JSON_KEY_DEPARTURES)
                                .getJSONArray(JJBTConstant.JSON_KEY_RESULT);
                        departFlights = new ArrayList<>();
                        for(int i = 0 ; i <  departFlightsJSONArray.length() ; i++){
                            JJBTFlightModel flightModel =
                                    parseFlightFromJson(departFlightsJSONArray.getJSONObject(i));
                            departFlights.add(flightModel);
                        }

                        JSONArray returnFlightsJSONArray = responseJSONObject
                                .getJSONObject(JJBTConstant.JSON_KEY_RETURNS)
                                .getJSONArray(JJBTConstant.JSON_KEY_RESULT);
                        returnFlights = new ArrayList<>();
                        for(int i = 0 ; i <  returnFlightsJSONArray.length() ; i++){
                            JJBTFlightModel flightModel =
                                    parseFlightFromJson(returnFlightsJSONArray.getJSONObject(i));
                            returnFlights.add(flightModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        String message = "Search Airport Failed";
                        result.setIsError(true);
                        result.setMessage(message);
                    }
                    return result;
                }

                @Override
                public JJUConnectionResultModel onHandleFailedData(String s) {
                    JJUConnectionResultModel result = new JJUConnectionResultModel("Failed to search flight",false);
                    return result;
                }

                @Override
                public void onUpdateUI(JJUConnectionResultModel result) {
                    if(listener != null){
                        if(result.isError() || departFlights == null){
                            listener.onRequestFailed("Failed to search flight");
                        }else{
                            listener.onRequestSuccess(departFlights, returnFlights);
                        }
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            String message = "Search Airport Failed";
            listener.onRequestFailed(message);
        }
    }


    private JJBTFlightModel parseFlightFromJson(JSONObject response) throws JSONException {
        String flightId = response.getString(JJBTConstant.JSON_KEY_FLIGHT_ID);
        String flightAirlinesName = response.getString(JJBTConstant.JSON_KEY_AIRLINES_NAME);
        String flightDepartureTime = response.getString(JJBTConstant.JSON_KEY_SIMPLE_DEPARTURE_TIME);
        String flightArrivalTime = response.getString(JJBTConstant.JSON_KEY_SIMPLE_ARRIVAL_TIME);
        int flightDuration = JJBTDurationHelper.getFlightDuration(response.getString(JJBTConstant.JSON_KEY_DURATION));
        double flightRealPrice = response.getDouble(JJBTConstant.JSON_KEY_PRICE_VALUE);
        double flightTotalPrice = 0.00;

        return new JJBTFlightModel(flightId,flightAirlinesName,
                flightDepartureTime,flightArrivalTime,
                flightDuration,flightTotalPrice,flightRealPrice);
    }

}
