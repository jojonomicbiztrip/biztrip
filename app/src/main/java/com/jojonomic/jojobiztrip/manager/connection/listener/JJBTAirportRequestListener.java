package com.jojonomic.jojobiztrip.manager.connection.listener;

import com.jojonomic.jojobiztrip.model.JJBTAirportModel;

import java.util.List;

/**
 * @author Muhammad Umar Farisi
 * @created 07/04/2017
 */
public interface JJBTAirportRequestListener {
    void onRequestSuccess(List<JJBTAirportModel> models);
    void onRequestFailed(String message);
}
