package com.jojonomic.jojobiztrip.manager.connection.listener;

import com.jojonomic.jojobiztrip.model.JJBTFlightModel;

import java.util.List;

/**
 * @author Muhammad Umar Farisi
 * @created 07/04/2017
 */
public interface JJBTFlightRequestListener {
    void onRequestSuccess(List<JJBTFlightModel> depart, List<JJBTFlightModel> returns);
    void onRequestFailed(String message);
}
