package com.jojonomic.jojobiztrip.model;

/**
 * @author Muhammad Umar Farisi
 * @created 12/04/2017
 */
public class JJBTContactModel {

    private String contactName;
    private String contactNumber;
    private String contactEmail;

    public JJBTContactModel(){
        this(null,null,null);
    }

    public JJBTContactModel(String contactName, String contactNumber, String contactEmail) {
        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.contactEmail = contactEmail;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JJBTContactModel that = (JJBTContactModel) o;

        return contactNumber.equals(that.contactNumber);

    }

    @Override
    public int hashCode() {
        return contactNumber.hashCode();
    }

    @Override
    public String toString() {
        return "JJBTContactModel{" +
                "contactName='" + contactName + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                '}';
    }
}
