package com.jojonomic.jojobiztrip.model;

import java.io.Serializable;

/**
 * @author Muhammad Umar Farisi
 * @created 30/03/2017
 */
public class JJBTAirportModel implements Serializable {

    private String airportName;
    private String airportCode;
    private String airportLocationName;
    private String airportCountryId;

    public JJBTAirportModel(){

    }

    public JJBTAirportModel(String airportName, String airportCode, String airportLocationName, String airportCountryId) {
        this.airportName = airportName;
        this.airportCode = airportCode;
        this.airportLocationName = airportLocationName;
        this.airportCountryId = airportCountryId;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportLocationName() {
        return airportLocationName;
    }

    public void setAirportLocationName(String airportLocationName) {
        this.airportLocationName = airportLocationName;
    }

    public String getAirportCountryId() {
        return airportCountryId;
    }

    public void setAirportCountryId(String airportCountryId) {
        this.airportCountryId = airportCountryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JJBTAirportModel that = (JJBTAirportModel) o;

        return airportCode.equals(that.airportCode);

    }

    @Override
    public int hashCode() {
        return airportCode.hashCode();
    }

    @Override
    public String toString() {
        return airportName;
    }
}
