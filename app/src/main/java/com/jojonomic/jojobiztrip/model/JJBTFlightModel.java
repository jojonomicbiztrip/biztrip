package com.jojonomic.jojobiztrip.model;

import java.io.Serializable;

/**
 * Created by Asus on 24/02/2017.
 */
public class JJBTFlightModel implements Serializable{

    private String flightId;
    private String flightAirlinesName;
    private String flightDepartureTime;
    private String flightArrivalTime;
    private int flightDuration;
    private double flightTotalPrice;
    private double flightRealPrice;

    public JJBTFlightModel(String flightId, String flightAirlinesName
            , String flightDepartureTime, String flightArrivalTime
            , int flightDuration, double flightTotalPrice, double flightRealPrice) {
        this.flightId = flightId;
        this.flightAirlinesName = flightAirlinesName;
        this.flightDepartureTime = flightDepartureTime;
        this.flightArrivalTime = flightArrivalTime;
        this.flightDuration = flightDuration;
        this.flightTotalPrice = flightTotalPrice;
        this.flightRealPrice = flightRealPrice;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getFlightAirlinesName() {
        return flightAirlinesName;
    }

    public void setFlightAirlinesName(String flightAirlinesName) {
        this.flightAirlinesName = flightAirlinesName;
    }

    public String getFlightDepartureTime() {
        return flightDepartureTime;
    }

    public void setFlightDepartureTime(String flightDepartureTime) {
        this.flightDepartureTime = flightDepartureTime;
    }

    public String getFlightArrivalTime() {
        return flightArrivalTime;
    }

    public void setFlightArrivalTime(String flightArrivalTime) {
        this.flightArrivalTime = flightArrivalTime;
    }

    public int getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(int flightDuration) {
        this.flightDuration = flightDuration;
    }

    public double getFlightTotalPrice() {
        return flightTotalPrice;
    }

    public void setFlightTotalPrice(double flightTotalPrice) {
        this.flightTotalPrice = flightTotalPrice;
    }

    public double getFlightRealPrice() {
        return flightRealPrice;
    }

    public void setFlightRealPrice(double flightRealPrice) {
        this.flightRealPrice = flightRealPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JJBTFlightModel that = (JJBTFlightModel) o;

        return flightId.equals(that.flightId);

    }

    @Override
    public int hashCode() {
        return flightId.hashCode();
    }

    @Override
    public String toString() {
        return "JJBTFlightModel{" +
                "flightId='" + flightId + '\'' +
                ", flightAirlinesName='" + flightAirlinesName + '\'' +
                ", flightDepartureTime='" + flightDepartureTime + '\'' +
                ", flightArrivalTime='" + flightArrivalTime + '\'' +
                ", flightDuration='" + flightDuration + '\'' +
                ", flightTotalPrice='" + flightTotalPrice + '\'' +
                ", flightRealPrice='" + flightRealPrice + '\'' +
                '}';
    }
}
