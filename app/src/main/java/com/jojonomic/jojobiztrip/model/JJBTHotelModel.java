package com.jojonomic.jojobiztrip.model;

/**
 * Created by Asus on 01/03/2017.
 */
public class JJBTHotelModel {
    private String hotelName;
    private String hotelAddress;
    private int hotelRating;
    private String hotelOldPrice;
    private String hotelNewPrice;

    public JJBTHotelModel(String hotelName, String hotelAddress, int hotelRating, String hotelOldPrice, String hotelNewPrice) {
        this.hotelName = hotelName;
        this.hotelAddress = hotelAddress;
        this.hotelRating = hotelRating;
        this.hotelOldPrice = hotelOldPrice;
        this.hotelNewPrice = hotelNewPrice;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public String getHotelAddress() {
        return hotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        this.hotelAddress = hotelAddress;
    }

    public int getHotelRating() {
        return hotelRating;
    }

    public void setHotelRating(int hotelRating) {
        this.hotelRating = hotelRating;
    }

    public String getHotelOldPrice() {
        return hotelOldPrice;
    }

    public void setHotelOldPrice(String hotelOldPrice) {
        this.hotelOldPrice = hotelOldPrice;
    }

    public String getHotelNewPrice() {
        return hotelNewPrice;
    }

    public void setHotelNewPrice(String hotelNewPrice) {
        this.hotelNewPrice = hotelNewPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JJBTHotelModel that = (JJBTHotelModel) o;

        if (hotelRating != that.hotelRating) return false;
        if (!hotelName.equals(that.hotelName)) return false;
        if (!hotelAddress.equals(that.hotelAddress)) return false;
        if (!hotelOldPrice.equals(that.hotelOldPrice)) return false;
        return hotelNewPrice.equals(that.hotelNewPrice);

    }

    @Override
    public int hashCode() {
        int result = hotelName.hashCode();
        result = 31 * result + hotelAddress.hashCode();
        result = 31 * result + hotelRating;
        result = 31 * result + hotelOldPrice.hashCode();
        result = 31 * result + hotelNewPrice.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "JJBTHotelModel{" +
                "hotelName='" + hotelName + '\'' +
                ", hotelAddress='" + hotelAddress + '\'' +
                ", hotelRating=" + hotelRating +
                ", hotelOldPrice='" + hotelOldPrice + '\'' +
                ", hotelNewPrice='" + hotelNewPrice + '\'' +
                '}';
    }
}
