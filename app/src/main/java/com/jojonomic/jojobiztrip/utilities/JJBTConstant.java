package com.jojonomic.jojobiztrip.utilities;

/**
 * Created by Asus on 04/03/2017.
 */
public class JJBTConstant {

    public static final int RESULT_CODE_FILTER_FLIGHT = 1;
    public static final int RESULT_CODE_AIRPORT = 2;

    public static final String EXTRA_KEY_IS_ROUND_TRIP = "IsRoundTrip";
    public static final String EXTRA_KEY_PASSENGER_NAME = "PassengerName";
    public static final String EXTRA_KEY_PASSENGER_TITLE = "PassengerTitle";
    public static final String EXTRA_KEY_INDEX_OF_PASSENGER = "IndexOfPassenger";
    public static final String EXTRA_KEY_IS_FILTERED_BY_TIME = "IsFilteredByTime";
    public static final String EXTRA_KEY_IS_FILTERED_BY_PRICE = "IsFilteredByPrice";

    public static final String EXTRA_KEY_AIRPORT = "Airport";
    public static final String EXTRA_KEY_AIRPORT_QUERY = "AirportQuery";
    public static final String EXTRA_KEY_ORIGIN_AIRPORT = "OriginAirport";
    public static final String EXTRA_KEY_DESTINATION_AIRPORT = "DestinationAirport";
    public static final String EXTRA_KEY_DEPARTURE_DATE = "DepartureDate";
    public static final String EXTRA_KEY_RETURN_DATE = "ReturnDate";
    public static final String EXTRA_KEY_TOTAL_ADULT = "TotalAdult";
    public static final String EXTRA_KEY_TOTAL_CHILD = "TotalChild";
    public static final String EXTRA_KEY_TOTAL_INFANT = "TotalInfant";
    public static final String EXTRA_KEY_DESCRIPTION = "Description";

    public static final String EXTRA_KEY_DEPART_FLIGHT = "DepartFlight";
    public static final String EXTRA_KEY_RETURN_FLIGHT = "ReturnFlight";

    public static final String JSON_KEY_DIAGNOSTIC = "diagnostic";
    public static final String JSON_KEY_STATUS = "status";
    public static final String JSON_KEY_ALL_AIRPORT = "all_airport";
    public static final String JSON_KEY_AIRPORT = "airport";
    public static final String JSON_KEY_AIRPORT_NAME = "airport_name";
    public static final String JSON_KEY_AIRPORT_CODE = "airport_code";
    public static final String JSON_KEY_LOCATION_NAME = "location_name";
    public static final String JSON_KEY_COUNTRY_ID = "country_id";

    public static final String JSON_KEY_DEPARTURES = "departures";
    public static final String JSON_KEY_RETURNS = "returns";
    public static final String JSON_KEY_RESULT = "result";
    public static final String JSON_KEY_FLIGHT_ID = "flight_id";
    public static final String JSON_KEY_AIRLINES_NAME = "airlines_name";
    public static final String JSON_KEY_SIMPLE_DEPARTURE_TIME = "simple_departure_time";
    public static final String JSON_KEY_SIMPLE_ARRIVAL_TIME = "simple_arrival_time";
    public static final String JSON_KEY_DURATION = "duration";
    public static final String JSON_KEY_PRICE_VALUE = "price_value";

    public static final String JSON_KEY_DEPARTURE_AIRPORT_CODE = "d";
    public static final String JSON_KEY_ARRIVAL_AIRPORT_CODE = "a";
    public static final String JSON_KEY_DEPART_DATE = "date";
    public static final String JSON_KEY_TOTAL_ADULT = "adult";
    public static final String JSON_KEY_TOTAL_CHILD = "child";
    public static final String JSON_KEY_TOTAL_INFANT = "infant";
    public static final String JSON_KEY_RETURN_DATE = "re_date";
}
