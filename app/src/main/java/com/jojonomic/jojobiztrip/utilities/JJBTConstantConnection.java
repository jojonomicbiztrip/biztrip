package com.jojonomic.jojobiztrip.utilities;

/**
 * @author Muhammad Umar Farisi
 * @created 30/03/2017
 */
public class JJBTConstantConnection {
    public static final String URL_SEARCH_AIRPORT = "http://10.0.2.2/jjbt/airport.json";
    public static final String URL_SEARCH_FLIGHT = "http://10.0.2.2/jjbt/flight.json";
}
