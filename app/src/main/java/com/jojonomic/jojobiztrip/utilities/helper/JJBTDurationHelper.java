package com.jojonomic.jojobiztrip.utilities.helper;

/**
 * @author Muhammad Umar Farisi
 * @created 12/04/2017
 */
public class JJBTDurationHelper {

    private static final int TOTAL_MINUTE_IN_HOUR = 60;
    private static final int INDEX_OF_HOUR_IN_FLIGHT_DURATION = 0;
    private static final int INDEX_OF_MINUTE_IN_FLIGHT_DURATION = 2;

    public static int getFlightDuration(String flightDurationFormat){
        String[] flightDurationFormatSplit = flightDurationFormat.split(" ");
        return Integer.parseInt(flightDurationFormatSplit[INDEX_OF_HOUR_IN_FLIGHT_DURATION])*TOTAL_MINUTE_IN_HOUR
                + Integer.parseInt(flightDurationFormatSplit[INDEX_OF_MINUTE_IN_FLIGHT_DURATION]);
    }

    public static String getFlightDurationFormatted(int flightDuration){
        return flightDuration/TOTAL_MINUTE_IN_HOUR+" h "+flightDuration%TOTAL_MINUTE_IN_HOUR+" m";
    }

}
