package com.jojonomic.jojobiztrip.utilities.helper;

import com.jojonomic.jojobiztrip.model.JJBTAirportModel;

/**
 * @author Muhammad Umar Farisi
 * @created 18/04/2017
 */
public class JJBTAirportHelper {
    public static String getAirportVisibleFormatted(JJBTAirportModel model){
        return model.getAirportLocationName()+" - ("+model.getAirportCode()+")";
    }
}
